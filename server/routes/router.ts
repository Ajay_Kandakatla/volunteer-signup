import { Router, Request, Response } from 'express'
export const router = Router()

// Events route
import { eventsRouter } from './controllers/events.controller'
router.use('/api/events', eventsRouter)

// Form route
import { formOptionsRouter } from './controllers/form-options.controller'
router.use('/api/form-options', formOptionsRouter)
