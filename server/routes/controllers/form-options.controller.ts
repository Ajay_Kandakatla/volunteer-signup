import * as FormOptionsService from "../../services/form-options.service";
import { Router, Request as req, Response as res } from "express";

export const formOptionsRouter: Router = Router();

formOptionsRouter.get("/", async (req, res) => {
  let results = await FormOptionsService.getOptions();
  res.json(results);
});
