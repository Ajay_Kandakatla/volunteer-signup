import * as EventsService from '../../services/events.service'
import { Router, Request as req, Response as res } from 'express'

export const eventsRouter: Router = Router()

eventsRouter.post('/', async (req, res) => {
  try {
    let result = await EventsService.postEvent(req.body)
    // res.sendStatus(201);
    console.log('success', result)
    res.status(201).json(result)
  } catch (err) {
    res.sendStatus(500)
  }
})

eventsRouter.get('/country/:countryId', async (req, res) => {
  let results = await EventsService.getEventsByCountry(req.params.countryId)
  res.json(results)
})

eventsRouter.get('/:id', async (req, res) => {
  let result = await EventsService.getEventById(req.params.id)
  res.json(result)
})

eventsRouter.get('/stats/values', async (req, res) => {
  let country =
    req.query.country && req.query.country !== '' ? req.query.country : 'all'

  console.log('country', country)
  let employeeType =
    req.query.employeeType && req.query.employeeType !== ''
      ? req.query.employeeType
      : 'all'
  console.log('employee type', employeeType)

  let functionType =
    req.query.functionType && req.query.functionType !== ''
      ? req.query.functionType.toLowerCase()
      : 'all'

  let result
  if (req.query.date) {
    console.log('date', req.query.date)
    result = await EventsService.getStatsByDate(
      req.query.date,
      country,
      employeeType,
      functionType
    )
  } else if (req.query.day) {
    result = await EventsService.getStatsByDay(
      parseInt(req.query.day),
      country,
      employeeType,
      functionType
    )
  } else {
    console.log('no query')
    result = await EventsService.getStats(country, employeeType, functionType)
  }

  res.json(result)
})

eventsRouter.put('/:id', async (req, res) => {
  try {
    let result = await EventsService.putEvent(req.body, req.params.id)
    console.log('in controller update success')
    // res.sendStatus(201);
    console.log('success', result)
    res.status(201).json(result)
  } catch (err) {
    res.sendStatus(500)
  }
})

eventsRouter.put('/notes/:id', async (req, res) => {
  let result = await EventsService.putNotes(req.body, req.params.id)
  res.json(result)
})

eventsRouter.put('/assignee/:id', async (req, res) => {
  let result = await EventsService.putAssignee(req.body, req.params.id)
  res.json(result)
})

eventsRouter.put('/status/:id', async (req, res) => {
  let result = await EventsService.putStatus(req.body, req.params.id)
  res.json(result)
})
