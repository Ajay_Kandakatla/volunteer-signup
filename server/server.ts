import express from "express";
import express_session from "express-session";
import bodyParser from "body-parser";
import path from "path";
import cors from "cors";
const https = require("https");
const PUBLIC_PATH = process.argv[2]
  ? path.resolve(process.argv[2])
  : path.join(__dirname, "build");
const PORT = process.env.PORT || 8000;
const session = express_session({
  name: "server-session",
  secret: "genericSecret", // See Note #2
  saveUninitialized: true,
  resave: true
});
// const pcfsso = require("pcf-sso-express-middleware");

const app = express();

// Import the configurations and connect to database
import { Config } from "./config/config";
const config = Config.instance;

// PCF express session setup
app.use(session);
const externalURL = process.env.EXTERNAL_URL;

// Server setup
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Setting up CORS and pre-flight
let corsOptions: any = {
  origin: "http://localhost:3000",
  optionsSuccessStatus: 200,
  methods: ["GET", "PUT", "POST", "DELETE", "OPTIONS"],
  allowedHeaders: ["Content-Type"],
  credentials: true
};
app.use(cors(corsOptions));
const options = {
  dotfiles: "ignore",
  extensions: ["htm", "html"],
  index: false,
  redirect: false
  // setHeaders,
};
// Serving interface to client
app.use(express.static(PUBLIC_PATH, options));

function fileSender(path: any) {
  return (req: any, res: any) => {
    // setHeaders(res, path);
    res.sendFile(path);
  };
}

// Routes setup
import { router } from "./routes/router";

app.use(router);

// Static content
app.use("*", fileSender(path.join(PUBLIC_PATH, "index.html")));

process.on("unhandledRejection", (err) => {
  console.log("Uncaught Error", err);
});

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
// module.exports = app;
