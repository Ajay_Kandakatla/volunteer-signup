import { StatusTypesModel } from '../database/models/status-types.model'
import { EventTypesModel } from '../database/models/issue-types.model'
import { LocationsModel } from '../database/models/locations.model'
import { PrioritiesModel } from '../database/models/priorities.model'
import { IntakeModel } from '../database/models/intake.model'
import { EmploymentModel } from '../database/models/employment.model'

const statusModel = StatusTypesModel.instance
const issueTypesModel = EventTypesModel.instance
const locationsModel = LocationsModel.instance
const prioritiesModel = PrioritiesModel.instance
const intakeModel = IntakeModel.instance
const employmentModel = EmploymentModel.instance

export async function getOptions() {
  let results: any = {}
  let statuses = await statusModel.getAll()
  let priorities = await prioritiesModel.getAll()
  let issueTypes = await issueTypesModel.getAll()
  let locations = await locationsModel.getAll()
  let intakeMethods = await intakeModel.getAll()
  let employmentTypes = await employmentModel.getAll()

  results.statuses = statuses
  results.priorities = priorities
  results.issueTypes = issueTypes
  results.locations = locations
  results.intakeMethods = intakeMethods
  results.employmentTypes = employmentTypes

  return results
}
