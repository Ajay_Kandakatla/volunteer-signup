import { EventsModel } from '../database/models/events.model'
import { StatusTypesModel } from '../database/models/status-types.model'
import { EventTypesModel } from '../database/models/issue-types.model'
import { LocationsModel } from '../database/models/locations.model'
import { PrioritiesModel } from '../database/models/priorities.model'

const eventsModel = EventsModel.instance
const statusModel = StatusTypesModel.instance
const issueTypesModel = EventTypesModel.instance
const locationsModel = LocationsModel.instance
const prioritiesModel = PrioritiesModel.instance

/**
 * Handling post request by adding values
 * into respective database tables
 * @param values
 */
export async function postEvent(values: any) {
  try {
    await eventsModel.add(values)
    console.log('issue service success')
    return { status: 'success' }
  } catch (err) {
    throw err
  }
}

export async function getStats(
  country: string,
  employeeType: string,
  functionType: string
): Promise<any> {
  let result = await eventsModel.getStats(country, employeeType, functionType)
  return result
}

export async function getStatsByDate(
  date: string,
  country: string,
  employeeType: string,
  functionType: string
) {
  let currYear = new Date().getFullYear()
  let dateArr = date.split('')
  dateArr.splice(2, 0, '-')
  let formattedDate = currYear + '-' + dateArr.join('')

  let result = await eventsModel.getStatsByDate(
    formattedDate,
    country,
    employeeType,
    functionType
  )
  return result
}

export async function getStatsByDay(
  numDays: number,
  country: string,
  employeeType: string,
  functionType: string
) {
  let result = await eventsModel.getStatsByDay(
    numDays,
    country,
    employeeType,
    functionType
  )
  return result
}

export async function putEvent(values: any, id: string) {
  try {
    await eventsModel.updateEvent(values, id)
    return { status: 'success' }
  } catch (err) {
    throw err
  }
}

export async function putAssignee(values: any, id: any) {
  const { name } = values
  await eventsModel.addAssignee(name, id)
  return { status: 'success' }
}

export async function putNotes(values: any, id: string) {
  const { notes } = values
  await eventsModel.addNotes(notes, id)
  return { status: 'success' }
}

export async function putStatus(values: any, id: string) {
  const { status } = values
  await eventsModel.updateStatus(status, id)
  return { status: 'success' }
}

export async function getEventById(id: string) {
  let result = await eventsModel.getEventById(id)
  return result
}

export async function getEventsByCountry(countryId: any) {
  let columns: Array<any> = []
  let results: any = await eventsModel.getEventsByCountry(countryId)
  let ssoCounter: any = {}
  // To Set Count for SSO's
  results.forEach((x: any) => {
    if (ssoCounter[x.SSO] > 0) {
      ssoCounter[x.SSO]++
    } else {
      ssoCounter[x.SSO] = 1
    }
  })
  const updatedWithTimesKey = results.map((issue: any) => {
    //prettier-ignore
    return { ...issue, "times": ssoCounter[issue.SSO] }
  })

  let keys = Object.keys(results[0])
  let dropdownKeys = [
    'Location',
    'Event Type',
    'Priority',
    'Intake Method',
    'Status'
  ]
  let defaultColKeys = [
    'Event Id',
    'Status',
    'Assignee',
    'Notes',
    'Priority',
    'Associate Name',
    'SSO',
    'Associate Call Back Number',
    'Event Description',
    'Next Call Back Time'
  ]

  let statusFilterList = [
    'Unassigned',
    'In Progress',
    'First Attempt',
    'Second Attempt'
  ]
  for (const key of keys) {
    if (key !== 'Id') {
      columns.push({
        name: key,
        label: key,
        options: {
          filter: true,
          sort: true,
          filterType: dropdownKeys.includes(key) ? 'multiselect' : 'textField',
          filterList: key === 'Status' ? statusFilterList : [],
          display: defaultColKeys.includes(key)
        }
      })
    }
  }

  return { columns: columns, data: updatedWithTimesKey }
}
