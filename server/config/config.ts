import dotenv from "dotenv";
dotenv.config({ path: __dirname + "/.env" });

/**
 * Config class holds all the configurations for the
 * host and database
 */
export class Config {
  private static _instance: Config;
  // Environment fields
  private _nodeEnv: string;
  private _port: string;

  // Database fields
  private _dbHost: string;
  private _dbName: string;
  private _dbUser: string;
  private _dbPassword: string;
  private _dbPort: string;

  private constructor() {
    this._nodeEnv = process.env.NODE_ENV || "development";
    this._port = process.env.PORT || "8000";

    this._dbHost = process.env.DB_HOST || "localhost";
    this._dbName = process.env.DB_NAME || "db_name";
    this._dbUser = process.env.DB_USER || "user";
    this._dbPassword = process.env.DB_PASS || "password";
    this._dbPort = process.env.DB_PORT || "3307";
  }

  /**
   * Singleton getter so that we can keep the configurations
   * consistent
   */
  public static get instance(): Config {
    // Check if any instance already exists
    if (!Config._instance) {
      Config._instance = new Config();
    }
    return Config._instance;
  }

  // Getters & Setters
  public get nodeEnv(): string {
    return this._nodeEnv;
  }

  public get port(): string {
    return this._port;
  }

  public get dbHost(): string {
    return this._dbHost;
  }

  public get dbName(): string {
    return this._dbName;
  }

  public get dbUser(): string {
    return this._dbUser;
  }

  public get dbPassword(): string {
    return this._dbPassword;
  }

  public get dbPort(): string {
    return this._dbPort;
  }
}
