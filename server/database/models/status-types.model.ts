import { Database } from "../database";

export class StatusTypesModel {
  private static _instance: StatusTypesModel; // Singleton
  private _database: Database;
  private _tableName: string;
  private _idCol: string;
  private _nameCol: string;

  private constructor() {
    this._database = Database.instance;
    this._idCol = "status_id";
    this._tableName = "status_types";
    this._nameCol = "name";
  }

  public get idCol(): string {
    return this._idCol;
  }

  public get nameCol(): string {
    return this._nameCol;
  }

  public get tableName(): string {
    return this._tableName;
  }

  /*************************************************
   * Singleton
   ************************************************/
  public static get instance(): StatusTypesModel {
    if (!StatusTypesModel._instance) {
      StatusTypesModel._instance = new StatusTypesModel();
    }

    return StatusTypesModel._instance;
  }

  /**
   * Retrieving the status name based on the
   * status id
   * @param id
   */
  public async getStatusName(id: any) {
    let sql = `SELECT ${this._nameCol}
      FROM ${this._tableName}
      WHERE ${this._idCol} = '${id}'`;

    let result = await this._database.queryDB(sql, [], this._nameCol);
    return result[0];
  }

  public async getAll() {
    let sql = `SELECT ${this._nameCol}
      FROM ${this._tableName}`;

    return await this._database.queryDB(sql, [], this._nameCol);
  }

  public async getIdFromName(name: string) {
    let sql = `SELECT ${this._idCol}
      FROM ${this._tableName}
      WHERE ${this._nameCol} = '${name}'`;

    let result = await this._database.queryDB(sql, [], this._idCol);
    return result[0];
  }
}
