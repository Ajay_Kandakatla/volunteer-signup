import { Database } from '../database'
import { StatusTypesModel } from './status-types.model'
import { EventTypesModel } from './issue-types.model'
import { LocationsModel } from './locations.model'
import { PrioritiesModel } from './priorities.model'
import { IntakeModel } from './intake.model'
import { EmploymentModel } from './employment.model'
import mysql from 'mysql'
import moment from 'moment'
// TODO: Parameterize this -- 3/31/20 lydia.tse@syf.com
export class EventsModel {
  private static _instance: EventsModel // Singleton
  private _database: Database
  private _tableName: string

  // Column names
  private _idCol: string
  private _assigneeSSOCol: string
  private _assigneeNameCol: string
  private _associateNameCol: string
  private _associatePhoneCol: string
  private _associateEmailCol: string
  private _associateFunctionCol: string
  private _associateSSOCol: string
  private _descriptionCol: string
  private _notesCol: string
  private _locationIdCol: string
  private _typeIdCol: string
  private _priorityIdCol: string
  private _statusIdCol: string
  private _createTimeCol: string
  private _startedTimeCol: string
  private _completedTimeCol: string
  private _intakeIdCol: string
  private _updateTimeColumn: string
  private _spocCol: string
  private _techOwnerCol: string
  private _employmentIdCol: string
  private _callbackTimeCol: string

  // Models
  private _locationModel: LocationsModel
  private _priorityModel: PrioritiesModel
  private _typeModel: EventTypesModel
  private _statusModel: StatusTypesModel
  private _intakeModel: IntakeModel
  private _employmentModel: EmploymentModel

  private constructor() {
    this._database = Database.instance
    this._tableName = 'events'
    this._idCol = 'issue_id'
    this._assigneeSSOCol = 'assignee_sso'
    this._assigneeNameCol = 'assignee_name'
    this._associateSSOCol = 'associate_sso'
    this._associateNameCol = 'associate_name'
    this._associatePhoneCol = 'associate_phone'
    this._associateEmailCol = 'associate_email'
    this._associateFunctionCol = 'associate_function'
    this._descriptionCol = 'description'
    this._notesCol = 'notes'
    this._locationIdCol = 'location_id'
    this._priorityIdCol = 'priority_id'
    this._typeIdCol = 'type_id'
    this._statusIdCol = 'status_id'
    this._createTimeCol = 'created_time'
    this._startedTimeCol = 'started_time'
    this._completedTimeCol = 'completed_time'
    this._intakeIdCol = 'intake_id'
    this._callbackTimeCol = 'callback_time'
    this._locationModel = LocationsModel.instance
    this._priorityModel = PrioritiesModel.instance
    this._typeModel = EventTypesModel.instance
    this._statusModel = StatusTypesModel.instance
    this._intakeModel = IntakeModel.instance
    this._employmentModel = EmploymentModel.instance
    this._updateTimeColumn = 'last_updated'
    this._spocCol = 'spoc'
    this._techOwnerCol = 'tech_owner'
    this._employmentIdCol = 'employment_id'
  }

  /*************************************************
   * Singleton
   ************************************************/
  public static get instance(): EventsModel {
    if (!EventsModel._instance) {
      EventsModel._instance = new EventsModel()
    }

    return EventsModel._instance
  }

  /*************************************************
   * Adds the information to the database
   * @param values -- the values of the issue
   * @throws error if adding into the database
   * is unsuccessful
   ************************************************/
  public async add(values: any): Promise<any> {
    const {
      name,
      sso,
      callBackNumber,
      location,
      issueType,
      issueDescription,
      associateExternalEmail,
      associateFunction,
      priority,
      intakeMethod,
      spoc,
      techOwner,
      employmentTypes,
      nextCallbackTime
    } = values

    let locationId = await this._locationModel.getIdFromName(location)
    let typeId = await this._typeModel.getIdFromName(issueType)
    let priorityId = await this._priorityModel.getIdFromName(priority)
    let intakeId = await this._intakeModel.getIdFromName(intakeMethod)
    let employmentId = await this._employmentModel.getIdFromName(
      employmentTypes
    )

    let sql = `INSERT INTO events (
      associate_name,
      associate_sso,
      associate_phone,
      location_id,
      type_id,
      description,
      associate_email,
      associate_function,
      priority_id,
      intake_id,
      status_id, 
      spoc, 
      tech_owner, 
      employment_id, 
      ${this._callbackTimeCol})
    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`

    let args = [
      name,
      sso,
      callBackNumber,
      locationId,
      typeId,
      issueDescription,
      associateExternalEmail,
      associateFunction,
      priorityId,
      intakeId,
      1,
      spoc,
      techOwner,
      employmentId,
      nextCallbackTime
    ]

    try {
      await this._database.queryDB(sql, args)
    } catch (err) {
      throw err
    }
  }

  /*************************************************
   * _isInProgress: Helper function to determine
   * if an issue is already in progress
   * @param id -- the issue id
   * @returns boolean of in progress or not
   ************************************************/
  private async _isInProgress(id: string): Promise<any> {
    let sql = `SELECT ${this._statusIdCol} FROM events WHERE ${this._idCol} = ?`
    let result: any = await this._database.queryDB(sql, [id], this._statusIdCol)
    result = result[0]

    return result === 2
  }

  /************************************************
   * updateEvent: Updates the issue with the given
   * issue id in the database
   * @param values -- the values to be updated
   * @param id --  the issue id
   * @throws error when update is unsuccessful
   ***********************************************/
  public async updateEvent(values: any, id: string): Promise<any> {
    const {
      name,
      sso,
      callBackNumber,
      location,
      issueType,
      issueDescription,
      associateExternalEmail,
      associateFunction,
      priority,
      intakeMethod,
      assignee,
      notes,
      status,
      spoc,
      techOwner,
      employmentTypes,
      prevStatus,
      nextCallbackTime
    } = values

    // Mark as complete sql
    let markAsCompleteSql =
      status.toLowerCase() === 'complete' ||
      status.toLowerCase() === 'no response'
        ? `${this._completedTimeCol} = NOW(), `
        : ''

    let description = issueDescription
    let notesEscape = notes
    let locationId = await this._locationModel.getIdFromName(location)
    let typeId = await this._typeModel.getIdFromName(issueType)
    let priorityId = await this._priorityModel.getIdFromName(priority)
    let intakeId = await this._intakeModel.getIdFromName(intakeMethod)
    let updatedAssignee = status.toLowerCase() === 'unassigned' ? '' : assignee

    let sql = `UPDATE events
      SET associate_name = ?,
      associate_sso = ?, 
      associate_phone = ?, 
      location_id = ?, 
      type_id = ?, 
      description = ?, 
      associate_email = ?,
      associate_function = ?, 
      priority_id = ?, 
      intake_id = ?,
      notes = ?, 
      status_id = (SELECT status_id FROM status_types WHERE name = ?), 
      ${this._assigneeNameCol} = ?,
      ${this._updateTimeColumn} = NOW(), 
      ${markAsCompleteSql}
      ${this._spocCol} = ?,
      ${this._techOwnerCol} = ?, 
      ${this._employmentIdCol} = (SELECT employment_id FROM employment_types WHERE name = ?),
      ${this._callbackTimeCol} = ?
      WHERE issue_id= ?`

    let args = [
      name,
      sso,
      callBackNumber,
      locationId,
      typeId,
      description,
      associateExternalEmail,
      associateFunction,
      priorityId,
      intakeId,
      notesEscape,
      status,
      updatedAssignee,
      spoc,
      techOwner,
      employmentTypes,
      nextCallbackTime,
      id
    ]

    try {
      await this._database.queryDB(sql, args)
    } catch (err) {
      throw err
    }
  }

  /****************************************************
   *
   ***************************************************/
  public async addAssignee(name: any, id: any) {
    let statusId = await this._statusModel.getIdFromName('In Progress')
    let sql = `UPDATE ${this._tableName}
      SET ${this._assigneeNameCol} = '${name}',
      ${this._statusIdCol} = ${statusId}, 
      ${this._startedTimeCol} = NOW(),
      ${this._updateTimeColumn} = NOW()
      WHERE ${this._idCol} = ${id}`

    await this._database.queryDB(sql, [])
  }

  /****************************************************
   * getStatsByDay: Retrieves the relevant stats
   * based on the specified days
   * @param numDays
   * @param country
   * @param employeeType
   * @returns json with relevant stats
   ****************************************************/
  public async getStatsByDay(
    numDays: number,
    country: string,
    employeeType: string,
    functionType: string
  ): Promise<any> {
    let employeeTypeSql = this._getEmployeeTypeSql(employeeType)
    let selectCountrySql =
      country.toLowerCase() === 'all'
        ? ''
        : ` AND ${this._locationIdCol} IN
      (SELECT ${this._locationIdCol} 
        FROM ${this._locationModel.tableName} 
        WHERE country = '${country}')`

    let numEventsSql = `SELECT COUNT(*) as count
      FROM ${this._tableName}
      WHERE DATE(created_time) >= CURDATE() - INTERVAL ? DAY ${selectCountrySql} ${employeeTypeSql}`
    let numEventsResults = await this._database.queryDB(
      numEventsSql,
      [numDays],
      'count'
    )
    let statusResults = await this._getStatsByDayResults(
      numDays,
      this._statusModel.tableName,
      this._statusIdCol,
      'name',
      country,
      employeeType
    )
    let totalIncomplete = this._calcNumIncomplete(statusResults, 'count')

    let priorityResults = await this._getStatsByDayResults(
      numDays,
      this._priorityModel.tableName,
      this._priorityIdCol,
      'name',
      country,
      employeeType
    )

    let typesResults = await this._getStatsByDayResults(
      numDays,
      this._typeModel.tableName,
      this._typeIdCol,
      'name',
      country,
      employeeType
    )

    let locationResults = await this._getStatsByDayResults(
      numDays,
      this._locationModel.tableName,
      this._locationIdCol,
      'city',
      country,
      employeeType
    )

    let intakeResults = await this._getStatsByDayResults(
      numDays,
      this._intakeModel.tableName,
      this._intakeIdCol,
      'name',
      country,
      employeeType
    )

    return {
      numEvents: numEventsResults[0],
      totalIncomplete: totalIncomplete,
      status: this._formatSearchStats(
        statusResults,
        'name',
        numEventsResults[0]
      ),
      priority: this._formatSearchStats(
        priorityResults,
        'name',
        numEventsResults[0]
      ),
      issueTypes: this._formatSearchStats(
        typesResults,
        'name',
        numEventsResults[0]
      ),
      location: this._formatSearchStats(
        locationResults,
        'city',
        numEventsResults[0]
      ),
      intake: this._formatSearchStats(
        intakeResults,
        'name',
        numEventsResults[0]
      )
    }
  }

  /*************************************************
   *  _getStatsByDayResults
   * @param numDays
   * @param searchTableName
   * @param columnName
   * @param nameCol
   * @param country
   * @param employeeType
   ************************************************/
  private async _getStatsByDayResults(
    numDays: number,
    searchTableName: string,
    columnName: string,
    nameCol: string,
    country: string,
    employeeType: string
  ) {
    let employeeTypeSql = this._getEmployeeTypeSql(employeeType)
    let selectCountrySql =
      country.toLowerCase() === 'all'
        ? ''
        : ` AND ${this._locationIdCol} IN
      (SELECT ${this._locationIdCol} 
        FROM ${this._locationModel.tableName} 
        WHERE country = '${country}')`

    let sql = `SELECT t1.${columnName}, t1.${nameCol}, 
    (SELECT COUNT(${columnName}) from ${this._tableName}
    WHERE t1.${columnName} = ${columnName}
    AND DATE(created_time) >= CURDATE() - INTERVAL ? DAY ${selectCountrySql} ${employeeTypeSql}) as count,
    (SELECT COUNT(*) FROM ${this._tableName} WHERE ${this._tableName}.${columnName} IS NULL
    AND DATE(created_time) >= CURDATE() - INTERVAL ? DAY ${selectCountrySql} ${employeeTypeSql}) as noResponse
    FROM ${searchTableName} as t1
    GROUP BY t1.${columnName}
    ORDER BY t1.${columnName} DESC`

    return await this._database.queryDB(sql, [numDays, numDays])
  }

  /**
   *
   */
  public async getStatsByDate(
    date: string,
    country: string,
    employeeType: string,
    functionType: string
  ) {
    let employeeTypeSql = this._getEmployeeTypeSql(employeeType)
    let functionTypeSql = this._getAssociateFunctionSql(functionType)

    let selectCountrySql =
      country.toLowerCase() === 'all'
        ? ''
        : ` AND ${this._locationIdCol} IN
      (SELECT ${this._locationIdCol} 
        FROM ${this._locationModel.tableName} 
        WHERE country = '${country}')`

    let numEventsSql = `SELECT COUNT(*) as count
      FROM ${this._tableName}
      WHERE DATE(${this._createTimeCol}) = DATE(?) ${selectCountrySql} ${employeeTypeSql} ${functionTypeSql}`
    let numEventsResults = await this._database.queryDB(
      numEventsSql,
      [date],
      'count'
    )

    let statusResults = await this._getStatsByDateResults(
      date,
      this._statusModel.tableName,
      this._statusIdCol,
      'name',
      country,
      employeeType,
      functionType
    )
    let totalIncomplete = this._calcNumIncomplete(statusResults, 'count')

    let priorityResults = await this._getStatsByDateResults(
      date,
      this._priorityModel.tableName,
      this._priorityIdCol,
      'name',
      country,
      employeeType,
      functionType
    )

    let typesResults = await this._getStatsByDateResults(
      date,
      this._typeModel.tableName,
      this._typeIdCol,
      'name',
      country,
      employeeType,
      functionType
    )

    let locationResults = await this._getStatsByDateResults(
      date,
      this._locationModel.tableName,
      this._locationIdCol,
      'city',
      country,
      employeeType,
      functionType
    )

    let intakeResults = await this._getStatsByDateResults(
      date,
      this._intakeModel.tableName,
      this._intakeIdCol,
      'name',
      country,
      employeeType,
      functionType
    )

    let hourData = await this._getCreatedTimeData(
      date,
      selectCountrySql,
      employeeTypeSql
    )

    return {
      numEvents: numEventsResults[0],
      totalIncomplete: totalIncomplete,
      status: this._formatSearchStats(
        statusResults,
        'name',
        numEventsResults[0]
      ),
      priority: this._formatSearchStats(
        priorityResults,
        'name',
        numEventsResults[0]
      ),
      issueTypes: this._formatSearchStats(
        typesResults,
        'name',
        numEventsResults[0]
      ),
      location: this._formatSearchStats(
        locationResults,
        'city',
        numEventsResults[0]
      ),
      intake: this._formatSearchStats(
        intakeResults,
        'name',
        numEventsResults[0]
      ),
      hourCountData: hourData
    }
  }

  /*********************************************
   * _getStatsByDateResult: Returns the stats
   * for a particular date based on the
   * filter criteria
   * @param date -- date string to be searched
   * @param searchTableName
   * @param columnName
   * @param nameCol
   * @param country
   * @param employeeType
   * @return results of the sql query
   ********************************************/
  private async _getStatsByDateResults(
    date: string,
    searchTableName: string,
    columnName: string,
    nameCol: string,
    country: string,
    employeeType: string,
    functionType: string
  ) {
    let employeeTypeSql = this._getEmployeeTypeSql(employeeType)
    let functionTypeSql = this._getAssociateFunctionSql(functionType)

    let selectCountrySql =
      country.toLowerCase() === 'all'
        ? ''
        : ` AND ${this._locationIdCol} IN
      (SELECT ${this._locationIdCol} 
        FROM ${this._locationModel.tableName} 
        WHERE country = '${country}')`

    let sql = `SELECT t1.${columnName}, t1.${nameCol}, 
    (SELECT COUNT(${columnName}) from ${this._tableName}
    WHERE t1.${columnName} = ${columnName}
    AND DATE(${this._createTimeCol}) = DATE(?) ${selectCountrySql} ${employeeTypeSql} ${functionTypeSql}) as count,
    (SELECT COUNT(*) FROM ${this._tableName} WHERE ${this._tableName}.${columnName} IS NULL
    AND DATE(created_time) =  DATE(?) ${selectCountrySql} ${employeeTypeSql} ${functionTypeSql}) as noResponse
    FROM ${searchTableName} as t1
    GROUP BY t1.${columnName}
    ORDER BY t1.${columnName} DESC`

    return await this._database.queryDB(sql, [date, date])
  }

  private async _getCreatedTimeData(
    date: string,
    selectCountrySql: string,
    employeeTypeSql: string
  ) {
    let sql = `SELECT TIME_FORMAT(CONCAT(hr_range.range_hour, ':00'), '%h:%i %p') as hour,
    (SELECT COUNT(issue_id)
    FROM events
    WHERE HOUR(events.created_time) = hr_range.range_hour
      AND DATE(created_time) = DATE(?)
    ) as currentDate,
    (SELECT COUNT(issue_id)
    FROM events
    WHERE HOUR(events.created_time) = hr_range.range_hour
      AND DATE(created_time) = DATE_SUB(?, INTERVAL 1 DAY)
    ) as oneDayAgo
    FROM hr_range
    ORDER BY STR_TO_DATE(hour, '%h:%i %p')`

    let results: Array<any> = await this._database.queryDB(sql, [date, date])
    results = results.map((result) => ({
      hour: result.hour,
      currentDate: result.currentDate,
      oneDayAgo: result.oneDayAgo
    }))

    return results
  }

  /************************************************
   * _formateSearchStats: Formats the
   * results generated from the stats into an
   * array of json objects
   * @param results - the results from a query
   * @param nameCol - the column that holds the name
   * @param numEvents - the total number of events
   ************************************************/
  private _formatSearchStats(
    results: Array<any>,
    nameCol: string,
    numEvents: number
  ): Array<any> {
    let formattedResults: Array<any> = results.map((result) => ({
      name: result[nameCol],
      count: result.count,
      percentage: this._calcPercentage(result.count, numEvents)
    }))

    formattedResults.push({
      name: 'Total Unknown',
      count: results[0].noResponse,
      percentage: this._calcPercentage(results[0].noResponse, numEvents)
    })

    return formattedResults
  }

  /************************************************
   * _filterSql: Helper function that generates
   * the sql query to inject if there is a filter
   * @param filterType
   * @param columnName
   * @param tableName
   * @returns string of sql query
   ************************************************/
  private _filterSql(
    filterType: string,
    columnName: string,
    tableName: string
  ): string {
    let sql =
      filterType.toLowerCase() === 'all'
        ? ''
        : ` AND ${columnName} =
      (SELECT ${columnName} 
        FROM ${tableName} 
        WHERE name = '${filterType}')`

    return sql
  }

  /**
   *
   */
  private _getEmployeeTypeSql(employmentType: string) {
    let ssoSql: string
    if (employmentType.toLowerCase() === 'contractor') {
      ssoSql = ` OR ${this._associateSSOCol} LIKE '5%') `
    } else {
      ssoSql = ` OR ${this._associateSSOCol} LIKE '2%') `
    }

    let sql =
      employmentType.toLowerCase() === 'all'
        ? ''
        : ` AND (${this._employmentIdCol} = 
      (SELECT ${this._employmentIdCol} FROM ${this._employmentModel.tableName}
        WHERE name = '${employmentType}') ${ssoSql} `

    return sql
  }

  /**
   *
   */
  private _getAssociateFunctionSql(functionType: string) {
    let sql: string
    if (functionType === 'all') {
      sql = ''
    } else {
      sql = ` AND ${this._associateFunctionCol} LIKE '%${functionType}%' `
    }

    return sql
  }

  /******************************************************
   * getStats -- Retrieves cumulative statistics based
   * on selected country or employment type
   * @param string
   * @param string
   ****************************************************/
  public async getStats(
    country: string,
    employeeType: string,
    functionType: string
  ) {
    let employeeTypeSql = this._getEmployeeTypeSql(employeeType)
    let functionTypeSql = this._getAssociateFunctionSql(functionType)

    let selectCountrySql =
      country.toLowerCase() === 'all'
        ? ''
        : ` AND ${this._locationIdCol} IN
        (SELECT ${this._locationIdCol} 
          FROM ${this._locationModel.tableName} 
          WHERE country = '${country}')`

    let whereSql =
      country.toLowerCase() === 'all' &&
      employeeType.toLowerCase() === 'all' &&
      functionType.toLowerCase() === 'all'
        ? ''
        : ' WHERE '

    let numEventsCountrySql =
      country.toLowerCase() === 'all'
        ? ''
        : ` ${this._locationIdCol} IN
    (SELECT ${this._locationIdCol} 
      FROM ${this._locationModel.tableName} 
      WHERE country = '${country}')`

    let numEventsEmployeeSql: string
    if (employeeType.toLowerCase() === 'all') {
      numEventsEmployeeSql = ''
    } else {
      let ssoSql = employeeType.toLowerCase() === 'contractor' ? '5%' : '2%'
      let andSql = numEventsCountrySql !== '' ? ' AND ' : ''
      numEventsEmployeeSql = ` ${andSql} (${this._employmentIdCol} IN
        (SELECT ${this._employmentIdCol} 
          FROM ${this._employmentModel.tableName} 
          WHERE name = '${employeeType}') OR 
          ${this._associateSSOCol} LIKE '${ssoSql}') `
    }

    let numEventsFunctionSql: string
    if (functionType.toLowerCase() === 'all') {
      numEventsFunctionSql = ''
    } else {
      let andSql =
        numEventsEmployeeSql !== '' || numEventsCountrySql !== '' ? ' AND ' : ''
      numEventsFunctionSql = ` ${andSql} ${this._associateFunctionCol} LIKE '%${functionType}%' `
    }

    whereSql =
      whereSql +
      numEventsCountrySql +
      numEventsEmployeeSql +
      numEventsFunctionSql

    let numEventsSql = `SELECT
    (
      SELECT COUNT(*) as count FROM events 
      ${whereSql}
    ) as cumulative`

    let numEvents = await this._database.queryDB(numEventsSql)
    numEvents = numEvents[0]

    let statusResults = await this._getCumulativeStats(
      this._statusModel.idCol,
      this._statusModel.nameCol,
      this._statusModel.tableName,
      selectCountrySql,
      employeeTypeSql,
      functionTypeSql
    )

    let totalIncomplete = this._calcNumIncomplete(statusResults, 'cumulative')

    let priorityResults = await this._getCumulativeStats(
      this._priorityModel.idCol,
      this._priorityModel.nameCol,
      this._priorityModel.tableName,
      selectCountrySql,
      employeeTypeSql,
      functionTypeSql
    )

    let typeResults = await this._getCumulativeStats(
      this._typeModel.idCol,
      this._typeModel.nameCol,
      this._typeModel.tableName,
      selectCountrySql,
      employeeTypeSql,
      functionTypeSql
    )

    let locationResults = await this._getCumulativeStats(
      this._locationModel.idCol,
      this._locationModel.cityCol,
      this._locationModel.tableName,
      selectCountrySql,
      employeeTypeSql,
      functionTypeSql
    )

    let intakeResults = await this._getCumulativeStats(
      this._intakeModel.idCol,
      this._intakeModel.nameCol,
      this._intakeModel.tableName,
      selectCountrySql,
      employeeTypeSql,
      functionTypeSql
    )

    let keyStatsSql = `SELECT 
    (SELECT COUNT(*) 
    FROM events 
    WHERE DATE(completed_time) = CURDATE() ${selectCountrySql} ${employeeTypeSql} ${functionTypeSql}) AS completed, 
    (SELECT COUNT(*) 
    FROM events 
    WHERE DATE(created_time) = CURDATE() ${selectCountrySql} ${employeeTypeSql} ${functionTypeSql}) AS opened,
    (SELECT COUNT(*) FROM events
    WHERE status_id != 5 ${selectCountrySql}) as incomplete`
    let keyStats = await this._database.queryDB(keyStatsSql, [])
    keyStats = keyStats[0]

    let stats = {
      cumulative: {
        numEvents: numEvents.cumulative,
        totalIncomplete: totalIncomplete,
        status: this._formatStats(
          statusResults,
          numEvents,
          this._statusModel.nameCol
        ).cumulative,
        priority: this._formatStats(
          priorityResults,
          numEvents,
          this._priorityModel.nameCol
        ).cumulative,
        issueTypes: this._formatStats(
          typeResults,
          numEvents,
          this._typeModel.nameCol
        ).cumulative,
        location: this._formatStats(
          locationResults,
          numEvents,
          this._locationModel.cityCol
        ).cumulative,
        intake: this._formatStats(
          intakeResults,
          numEvents,
          this._intakeModel.nameCol
        ).cumulative
      },
      keyStats: {
        completedToday: keyStats.completed,
        openedToday: keyStats.opened,
        totalIncomplete: keyStats.incomplete
      }
    }
    return stats
  }

  private _calcNumIncomplete(results: Array<any>, colName: string) {
    let sum = 0
    for (const result of results) {
      if (result.name.toLowerCase() !== 'complete') {
        sum += result[colName]
      }
    }

    return sum
  }

  /***************************************************
   * _getCumulativeStats: performs cumulative stats
   * query for specified table and id
   * @param idCol
   * @param nameCol
   * @param tableName
   * @param selectCountrySql
   * @param employeeTypeSql
   * @returns An array of the rows that are returned from query
   **************************************************/
  private async _getCumulativeStats(
    idCol: string,
    nameCol: string,
    tableName: string,
    selectCountrySql: string,
    employeeTypeSql: string,
    functionTypeSql: string
  ): Promise<any> {
    let sql = `SELECT t1.${idCol}, t1.${nameCol}, 
    (SELECT COUNT(${idCol}) from ${this._tableName}
    WHERE t1.${idCol} = ${idCol} ${selectCountrySql} ${employeeTypeSql} ${functionTypeSql}) as cumulative,
    (SELECT COUNT(*) FROM ${this._tableName} WHERE ${this._tableName}.${idCol} IS NULL ${selectCountrySql} ${employeeTypeSql} ${functionTypeSql}) as noResponse
    FROM ${tableName} as t1
    GROUP BY t1.${idCol}
    ORDER BY t1.${idCol} DESC`
    return await this._database.queryDB(sql, [])
  }

  /**************************************************
   * _calcPercentage: Helper function that will
   * calculate the percentage value
   * @param number
   * @param number
   * @returns number -- the value of the percentage
   **************************************************/
  private _calcPercentage(count: number, total: number): number {
    return total === 0 ? 0 : (count / total) * 100
  }

  /***************************************************
   * _formatStats: Helper function to create an
   * array of json objects that will return
   * cumulative stats
   * @param results
   * @param numEvents
   **************************************************/
  private _formatStats(results: Array<any>, numEvents: any, nameCol: string) {
    let numUnknown: any = results[0].noResponse
    let stats = results.map((result: any) => ({
      name: result[nameCol],
      cumulative: result.cumulative
    }))

    let cumulativeArr: Array<any> = stats.map((stat: any) => ({
      name: stat.name,
      count: stat.cumulative,
      percentage: this._calcPercentage(stat.cumulative, numEvents.cumulative),
      noResponse: stat.noResponse
    }))
    cumulativeArr.push({
      name: 'Total Unknown',
      count: numUnknown,
      percentage: this._calcPercentage(numUnknown, numEvents.cumulative)
    })

    let info = {
      cumulative: cumulativeArr
    }

    return info
  }

  /****************************************************
   * getEventById: Retrieves the issue with corresponding
   * issue_id from the database
   * @param id
   * @returns json object containing isuse info
   ***************************************************/
  public async getEventById(id: string): Promise<any> {
    let sql = `SELECT ${this._tableName}.*,
      (SELECT city from locations where events.location_id = locations.location_id) as location, 
      (SELECT name from priorities where events.priority_id = priorities.priority_id) as priority, 
      (SELECT name from status_types where events.status_id = status_types.status_id) as status,
      (SELECT name from issue_types where events.type_id = issue_types.type_id) as issue,
      (SELECT name from intake_method where events.intake_id = intake_method.intake_id) as intake, 
      (SELECT name from employment_types where events.employment_id = employment_types.employment_id) as employmentType
      FROM ${this._tableName}
      WHERE ${this._idCol} = '${id}'`

    let result = await this._database.queryDB(sql, [])
    result = result[0]
    let info = {}
    if (result) {
      info = {
        name: result[this._associateNameCol],
        assignee: result[this._assigneeNameCol],
        status: result['status'],
        notes: result[this._notesCol],
        sso: result[this._associateSSOCol],
        phone: result[this._associatePhoneCol],
        description: result[this._descriptionCol],
        location: result['location'],
        issueType: result['issue'],
        email: result[this._associateEmailCol],
        function: result[this._associateFunctionCol],
        priority: result['priority'],
        intake: result['intake'],
        spoc: result[this._spocCol],
        techOwner: result[this._techOwnerCol],
        employmentType: result['employmentType'],
        nextCallbackTime: result[this._callbackTimeCol]
      }
    }

    return info
  }

  /*****************************************************
   * Adding note to database
   * @param notes
   * @param id
   ****************************************************/
  public async addNotes(notes: string, id: string) {
    let sql = `UPDATE ${this._tableName}
    SET ${this._notesCol} = ?
    WHERE ${this._idCol} = ${id}`

    await this._database.queryDB(sql, [notes])
  }

  /*****************************************************
   * updateStatus: Updates the status of a particular
   * issue
   ****************************************************/
  public async updateStatus(status: string, id: string) {
    let sql = `UPDATE ${this._tableName}
    SET ${this._statusIdCol} = (SELECT ${this._statusModel.idCol}
      FROM ${this._statusModel.tableName}
      WHERE ${this._statusModel.nameCol} = ?)
    WHERE ${this._idCol} = ${id}`

    // Mark as complete sql
    let markAsCompleteSql =
      status.toLowerCase() === 'complete' ||
      status.toLowerCase() === 'no response'
        ? `${this._completedTimeCol} = NOW(), `
        : ''

    if (status.toLowerCase() === 'unassigned') {
      sql = `UPDATE ${this._tableName}
        SET ${this._statusIdCol} = (SELECT ${this._statusModel.idCol}
         FROM ${this._statusModel.tableName}
         WHERE ${this._statusModel.nameCol} = ?),
        ${markAsCompleteSql}
        ${this._assigneeNameCol} = NULL
        WHERE ${this._idCol} = ${id}`
    }

    await this._database.queryDB(sql, [status])
  }

  /**
   * Retrieves all events by country from the database
   */
  public async getEventsByCountry(countryId: any) {
    let sql = `SELECT events.*, 
      (SELECT city from locations where events.location_id = locations.location_id) as location, 
      (SELECT name from priorities where events.priority_id = priorities.priority_id) as priority, 
      (SELECT name from status_types where events.status_id = status_types.status_id) as status,
      (SELECT name from issue_types where events.type_id = issue_types.type_id) as issue,
      (SELECT name from intake_method where events.intake_id = intake_method.intake_id) as intake,
      (SELECT cms_id from cms where events.associate_sso = cms.sso) as cms_id,
      (SELECT extension from cms where events.associate_sso = cms.sso) as extension,
      DATE_FORMAT(${this._updateTimeColumn}, '%m-%d-%Y %r' ) as 'updated',
      DATE_FORMAT(${this._createTimeCol}, '%m-%d-%Y %r' ) as 'created',
      (SELECT country from locations where events.location_id = locations.location_id) as country
      FROM events where location_id IN (SELECT location_id from locations where country_id = ${countryId})
      ORDER BY events.status_id ASC, issue_id ASC`
    let events: Array<any> = []
    let results = await this._database.queryDB(sql, [])

    // Formatting the events into json objects and storing
    for await (const result of results) {
      events.push({
        'Event Id': result[this._idCol],
        'Date Created': result['created'],
        Status: result['status'],
        Assignee: result[this._assigneeNameCol],
        Notes: result[this._notesCol],
        'Next Call Back Time': result[this._callbackTimeCol],
        Location: result['location'],
        'Last Updated': result['updated'] ? result['updated'] + ' EST' : '',
        Priority: result['priority'],
        'Associate Name': result[this._associateNameCol],
        SSO: result[this._associateSSOCol],
        'CMS Id': result['cms_id'],
        Extension: result['extension'],
        'Associate Call Back Number': result[this._associatePhoneCol],
        'Event Type': result['issue'],
        'Event Description': result[this._descriptionCol],
        'Associate External Email': result[this._associateEmailCol],
        'Associate Function': result[this._associateFunctionCol],
        'Intake Method': result['intake'],
        Id: result[this._idCol]
      })
    }

    return events
  }
}
