import { Database } from "../database";

export class EmploymentModel {
  private static _instance: EmploymentModel; // Singleton
  private _database: Database;
  private _tableName: string;
  private _idCol: string;
  private _nameCol: string;

  private constructor() {
    this._database = Database.instance;
    this._tableName = "employment_types";
    this._idCol = "employment_id";
    this._nameCol = "name";
  }

  /*************************************************
   * Singleton
   ************************************************/
  public static get instance(): EmploymentModel {
    if (!EmploymentModel._instance) {
      EmploymentModel._instance = new EmploymentModel();
    }

    return EmploymentModel._instance;
  }

  public get tableName(): string {
    return this._tableName;
  }

  /**
   * Retrieve
   * @param id
   */
  public async getNameFromId(id: any) {
    let sql = `SELECT ${this._nameCol}
      FROM ${this._tableName}
      WHERE ${this._idCol} = '${id}'`;

    let results = await this._database.queryDB(sql, [], this._nameCol);
    return results[0];
  }

  public async getAll() {
    let sql = `SELECT ${this._nameCol}
      FROM ${this._tableName}`;

    return await this._database.queryDB(sql, [], this._nameCol);
  }

  public async getIdFromName(name: string) {
    let sql = `SELECT ${this._idCol}
      FROM ${this._tableName}
      WHERE ${this._nameCol} = '${name}'`;

    let results = await this._database.queryDB(sql, [], this._idCol);
    return results[0];
  }
}
