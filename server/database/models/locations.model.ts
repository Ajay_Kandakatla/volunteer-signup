import { Database } from "../database";

export class LocationsModel {
  private static _instance: LocationsModel; // Singleton
  private _database: Database;
  private _tableName: string;
  private _idCol: string;
  private _cityCol: string;
  private _countryCol: string;
  private _countryId: string;

  private constructor() {
    this._database = Database.instance;
    this._tableName = "locations";
    this._idCol = "location_id";
    this._cityCol = "city";
    this._countryCol = "country";
    this._countryId = "country_id";
  }

  /*************************************************
   * Singleton
   ************************************************/
  public static get instance(): LocationsModel {
    if (!LocationsModel._instance) {
      LocationsModel._instance = new LocationsModel();
    }

    return LocationsModel._instance;
  }

  public get tableName(): string {
    return this._tableName;
  }

  public get idCol(): string {
    return this._idCol;
  }

  public get cityCol(): string {
    return this._cityCol;
  }

  /**
   * Retrieve the city name based on
   * location id
   * @param locationId
   */
  public async getCity(locationId: any) {
    let sql = `SELECT ${this._cityCol}
      FROM ${this._tableName}
      WHERE ${this._idCol} = '${locationId}'`;

    let result = await this._database.queryDB(sql, [], this._cityCol);
    return result[0];
  }

  /**
   *
   */
  public async getAll() {
    let sql = `SELECT ${this._cityCol}
      FROM ${this._tableName}
      ORDER BY ${this._cityCol} ASC`;

    return await this._database.queryDB(sql, [], this._cityCol);
  }

  public async getIdFromName(city: string) {
    let sql = `SELECT ${this._idCol}
      FROM ${this._tableName}
      WHERE ${this._cityCol} = '${city}'`;

    let result = await this._database.queryDB(sql, [], this._idCol);
    result = result[0];

    return result;
  }
}
