import { Database } from "../database";

export class PrioritiesModel {
  private static _instance: PrioritiesModel; // Singleton
  private _database: Database;
  private _tableName: string;
  private _idCol: string;
  private _nameCol: string;

  private constructor() {
    this._database = Database.instance;
    this._tableName = "priorities";
    this._idCol = "priority_id";
    this._nameCol = "name";
  }

  /*************************************************
   * Singleton Instance Getter
   ************************************************/
  public static get instance(): PrioritiesModel {
    if (!PrioritiesModel._instance) {
      PrioritiesModel._instance = new PrioritiesModel();
    }

    return PrioritiesModel._instance;
  }

  public get tableName(): string {
    return this._tableName;
  }

  public get idCol(): string {
    return this._idCol;
  }

  public get nameCol(): string {
    return this._nameCol;
  }

  /**
   *
   * @param id
   */
  public async getPriorityName(id: any) {
    let sql = `SELECT ${this._nameCol}
      FROM ${this._tableName}
      WHERE ${this._idCol} = '${id}'`;

    let result = await this._database.queryDB(sql, [], this._nameCol);
    return result[0];
  }

  public async getAll() {
    let sql = `SELECT ${this._nameCol}
      FROM ${this._tableName}`;

    return await this._database.queryDB(sql, [], this._nameCol);
  }

  public async getIdFromName(name: string) {
    let sql = `SELECT ${this._idCol}
      FROM ${this._tableName}
      WHERE ${this._nameCol} = '${name}'`;

    let result = await this._database.queryDB(sql, [], this._idCol);
    return result[0];
  }
}
