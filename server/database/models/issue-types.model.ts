import { Database } from '../database'

export class EventTypesModel {
  private static _instance: EventTypesModel // Singleton
  private _database: Database
  private _tableName: string
  private _idCol: string
  private _nameCol: string

  private constructor() {
    this._database = Database.instance
    this._tableName = 'issue_types'
    this._idCol = 'type_id'
    this._nameCol = 'name'
  }

  /*************************************************
   * Singleton
   ************************************************/
  public static get instance(): EventTypesModel {
    if (!EventTypesModel._instance) {
      EventTypesModel._instance = new EventTypesModel()
    }

    return EventTypesModel._instance
  }

  public get tableName(): string {
    return this._tableName
  }

  public get idCol(): string {
    return this._idCol
  }

  public get nameCol(): string {
    return this._nameCol
  }
  /**
   * Retrieve
   * @param id
   */
  public async getTypeName(id: any) {
    let sql = `SELECT ${this._nameCol}
      FROM ${this._tableName}
      WHERE ${this._idCol} = '${id}'`

    let result = await this._database.queryDB(sql, [], this._nameCol)
    return result[0]
  }

  public async getAll() {
    let sql = `SELECT ${this._nameCol}
      FROM ${this._tableName}`

    return await this._database.queryDB(sql, [], this._nameCol)
  }

  public async getIdFromName(name: string) {
    let sql = `SELECT ${this._idCol}
      FROM ${this._tableName}
      WHERE ${this._nameCol} = '${name}'`

    let result = await this._database.queryDB(sql, [], this._idCol)
    return result[0]
  }
}
