import { Database } from "../database";

export class IntakeModel {
  private static _instance: IntakeModel; // Singleton
  private _database: Database;
  private _tableName: string;
  private _idCol: string;
  private _nameCol: string;

  private constructor() {
    this._database = Database.instance;
    this._tableName = "intake_method";
    this._idCol = "intake_id";
    this._nameCol = "name";
  }

  /*************************************************
   * Singleton
   ************************************************/
  public static get instance(): IntakeModel {
    if (!IntakeModel._instance) {
      IntakeModel._instance = new IntakeModel();
    }

    return IntakeModel._instance;
  }

  public get tableName(): string {
    return this._tableName;
  }

  public get idCol(): string {
    return this._idCol;
  }

  public get nameCol(): string {
    return this._nameCol;
  }

  /**
   * Retrieve
   * @param id
   */
  public async getNameFromId(id: any) {
    let sql = `SELECT ${this._nameCol}
      FROM ${this._tableName}
      WHERE ${this._idCol} = '${id}'`;

    let results = await this._database.queryDB(sql, [], this._nameCol);
    return results[0];
  }

  public async getAll() {
    let sql = `SELECT ${this._nameCol}
      FROM ${this._tableName}`;

    return await this._database.queryDB(sql, [], this._nameCol);
  }

  public async getIdFromName(name: string) {
    let sql = `SELECT ${this._idCol}
      FROM ${this._tableName}
      WHERE ${this._nameCol} = '${name}'`;

    let results = await this._database.queryDB(sql, [], this._idCol);
    return results[0];
  }
}
