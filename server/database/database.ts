import mysql from 'mysql'
import { Config } from '../config/config'

export class Database {
  private static _instance: Database
  private _connectionConfig: any
  private _pool: any

  private constructor() {
    const config = Config.instance

    this._connectionConfig = {
      host: config.dbHost,
      user: config.dbUser,
      password: config.dbPassword,
      database: config.dbName,
      port: config.dbPort
    }

    this._pool = mysql.createPool(this._connectionConfig)
  }

  /**
   * Singleton instance getter
   */
  public static get instance(): Database {
    if (!Database._instance) {
      Database._instance = new Database()
    }

    return Database._instance
  }

  public get pool(): any {
    return this._pool
  }

  /**
   *
   */
  public queryDB(
    sql: string,
    args?: Array<any>,
    returnColName?: any
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      this._pool.query(sql, args, (err: any, rows: Array<any>) => {
        if (err) {
          console.log(err)
          throw 'Database Error'
          // return reject(err);
        }

        let results: any
        if (returnColName) {
          results = []
          rows.forEach((row: Array<any>) => results.push(row[returnColName]))
        } else {
          results = rows
        }
        resolve(results)
      })
    })
  }
}
