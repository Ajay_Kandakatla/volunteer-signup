import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App.jsx";
import { MuiThemeProvider, withStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import theme from "./styles/theme";

const GlobalCss = withStyles({
  // @global is handled by jss-plugin-global.

})(() => null);

ReactDOM.render(
  <MuiThemeProvider theme={theme}>
    <CssBaseline />
    <GlobalCss/>
    <App />
  </MuiThemeProvider>,
  document.getElementById("root")
);
