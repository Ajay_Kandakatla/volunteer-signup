import SynchronySansWoff from "./woff/SynchronySans-Normal.woff";
import SynchronySansWoff2 from "./woff2/SynchronySans-Normal.woff2";
import SynchronySansBoldWoff from "./woff/SynchronySans-Bold.woff";
import SynchronySansBoldWoff2 from "./woff2/SynchronySans-Bold.woff2";
import SynchronySansThinWoff from "./woff/SynchronySans-Thin.woff";
import SynchronySansThinWoff2 from "./woff2/SynchronySans-Thin.woff2";
import SynchronySansMediumWoff from "./woff/SynchronySans-Medium.woff";
import SynchronySansMediumWoff2 from "./woff2/SynchronySans-Medium.woff2";

export const synchronysans = {
  fontFamily: "Synchrony Sans",
  fontStyle: "normal",
  fontDisplay: "swap",
  fontWeight: "normal",
  src: `
    local('Synchrony-Sans),
    local('Synchrony-Sans-Regular) 
    url(${SynchronySansWoff}) format('woff'),
    url(${SynchronySansWoff2}) format('woff2')
  `
};

export const synchronysansBold = {
  fontFamily: "Synchrony Sans",
  fontStyle: "normal",
  fontDisplay: "swap",
  fontWeight: "bold",
  src: `
  local('Synchrony-Sans),
  local('Synchrony-Sans-Bold) 
  url(${SynchronySansBoldWoff}) format('woff'),
  url(${SynchronySansBoldWoff2}) format('woff2')
`
};

export const synchronysansThin = {
  fontFamily: "Synchrony Sans",
  fontStyle: "normal",
  fontDisplay: "swap",
  fontWeight: 200,
  src: `
  local('Synchrony-Sans),
  local('Synchrony-Sans-Thin) 
  url(${SynchronySansThinWoff}) format('woff'),
  url(${SynchronySansThinWoff2}) format('woff2')
`
};

export const synchronysansMedium = {
  fontFamily: "Synchrony Sans",
  fontStyle: "normal",
  fontDisplay: "swap",
  fontWeight: 500,
  src: `
  local('Synchrony-Sans),
  local('Synchrony-Sans-Medium) 
  url(${SynchronySansMediumWoff}) format('woff'),
  url(${SynchronySansMediumWoff2}) format('woff2')
`
};
