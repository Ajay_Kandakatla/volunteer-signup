import { createMuiTheme, responsiveFontSizes } from "@material-ui/core/styles";
// import {
//   synchronysans,
//   synchronysansBold,
//   synchronysansThin,
//   synchronysansMedium
// } from "./fonts/Synchrony-Sans-Font";
import { fontSize } from "@material-ui/system";

export const colors = {
  syfGreen: "#3E8529",
  syfGold: "#FBC600",
  charcoalGray: "#3B3C43",
  steelGray: "#94969A",
  teal: "#34657F",
  turqoise: "#58A7AF",
  white: "#FFFFFF",
  black: "#000000",
  darkgreen: "#3E8529",
  lightgreen: "#6EBF4A",
  errorRed: "#cc0000",
  lightPink: "#FFEBE6",
  pink: "#FFD8D6",
  lightYellow: "#FFF1B5",
};

let theme = createMuiTheme({
  typography: {
    // fontFamily: "Synchrony Sans, Arial",
    h1: {
      fontWeight: 200,
      fontSize: "3rem",
    },
    h2: {
      fontWeight: 200,
      fontSize: "2.5rem",
    },
    h3: {
      fontWeight: 200,
      fontSize: "2rem",
    },
    h4: { textTransform: "uppercase", fontSize: "1.5rem" },
    h5: { fontSize: "1.00rem" },
    button: { textTransform: "uppercase" },
  },
  palette: {
    primary: {
      main: colors.charcoalGray,
      contrastText: colors.white,
    },
    secondary: {
      main: colors.syfGold,
      contrastText: colors.black,
    },
    error: {
      main: colors.errorRed,
      contrastText: colors.white,
    },
    backgroundDefault: {
      main: colors.white,
      contrastText: colors.black,
    },
  },
  widths: {
    drawer: 240,
  },
});

// theme.overrides = {
//   ...theme.overrides,
//   MuiCssBaseline: {
//     "@global": {
//       "@font-face": [
//         synchronysans,
//         synchronysansBold,
//         synchronysansThin,
//         synchronysansMedium
//       ]
//     }
//   }
// };

// theme = responsiveFontSizes(theme);

export default theme;
