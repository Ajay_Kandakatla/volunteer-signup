import React, { useState, useEffect } from 'react'
import Dashboard from './Dashboard'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import { get } from '../../api/requests'
import LinearProgress from '@material-ui/core/LinearProgress'
import StatsSection from '../common/StatsSection'
import ExpansionPanel from '@material-ui/core/ExpansionPanel'
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import PrevStats from '../common/PrevStats'
import KeyStatsTable from '../common/KeyStatsTable'
import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography'
import { Button } from '@material-ui/core'
import { colors } from '../../styles/theme'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1
  },
  centered: {
    margin: 'auto'
  },
  heading: {
    fontSize: theme.typography.pxToRem(24),
    flexBasis: '16%',
    flexShrink: 0,
    color: colors.charcoalGray
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(24),
    color: colors.steelGray
  }
}))

const defaultProps = {
  bgcolor: 'background.paper',
  m: 1,
  border: 1,
  style: { width: '5rem', height: '5rem' }
}

export default function Stats() {
  const classes = useStyles()
  const [data, setData] = React.useState({})
  const [todayStats, setTodayStats] = React.useState({})
  const [contractorStats, setContractorStats] = useState({})
  const [ftStats, setFtStats] = useState({})
  const [loadingOne, setLoadingOne] = useState(true)
  const [loadingTwo, setLoadingTwo] = useState(true)

  var date = new Date()
  var day = date.getDate()
  // date returns just 5 and not 05
  if (day < 10) {
    day = '0' + day.toLocaleString()
    console.log('day', day)
  } else {
    day = day.toLocaleString()
  }

  var month = date.getMonth() + 1
  // returns just 4 and not 04 if month is April
  if (month < 10) {
    month = '0' + month.toLocaleString()
  } else {
    month = month.toLocaleString()
    console.log('month', month)
  }
  // api needs mmdd
  const mmdd_date = month + day
  console.log('today', mmdd_date)

  date = date.toLocaleString().split(',')[0]

  useEffect(() => {
    get(`/api/issues/stats/values?country=&employeeType=`)
      .then((res) => res.json())
      .then((res) => {
        setData(res)
        setLoadingOne(false)
      })
    get(`/api/issues/stats/values?date=${mmdd_date}`)
      .then((res) => res.json())
      .then((res) => {
        setTodayStats(res)
        setLoadingTwo(false)
      })
    get(`/api/issues/stats/values?employeeType=contractor&functionType=eo`)
      .then((res) => res.json())
      .then((res) => {
        setContractorStats(res)
      })
    get(`/api/issues/stats/values?employeeType=ft&functionType=eo`)
      .then((res) => res.json())
      .then((res) => {
        setFtStats(res)
      })
  }, [])

  return (
    <Dashboard currentPage="Stats">
      {loadingTwo || loadingOne ? (
        <LinearProgress />
      ) : (
        <div className={classes.root}>
          <Box mb={2}>
            <Grid container justify="center" spacing={1}>
              <Grid item>
                <KeyStatsTable
                  columns={['Key Stats', 'Count']}
                  rows={[
                    {
                      name:
                        'Total Completed Today (Includes Items Opened Prior to Today)',
                      count: data.keyStats.completedToday
                    },
                    {
                      name: 'Total Opened Today',
                      count: data.keyStats.openedToday
                    },
                    {
                      name:
                        'Total Currently Incomplete of All Submitted Issues',
                      count: data.keyStats.totalIncomplete
                    }
                  ]}
                />
              </Grid>
            </Grid>
          </Box>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1bh-content"
              id="panel1bh-header"
            >
              <Typography className={classes.heading}>
                Cumulative Statistics
              </Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <StatsSection data={data.cumulative} />
            </ExpansionPanelDetails>
          </ExpansionPanel>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1bh-content"
              id="panel1bh-header"
            >
              <Typography className={classes.heading}>
                Cumulative Statistics for EO Full Time Employees
              </Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <StatsSection data={ftStats.cumulative} />
            </ExpansionPanelDetails>
          </ExpansionPanel>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1bh-content"
              id="panel1bh-header"
            >
              <Typography className={classes.heading}>
                Cumulative Statistics for EO Contractors
              </Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <StatsSection data={contractorStats.cumulative} />
            </ExpansionPanelDetails>
          </ExpansionPanel>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1bh-content"
              id="panel1bh-header"
            >
              <Typography className={classes.heading}>
                Statistics for Issues Opened Today
              </Typography>
              <Typography className={classes.secondaryHeading}>
                {date}
              </Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <StatsSection data={todayStats} includesBarChart />
            </ExpansionPanelDetails>
          </ExpansionPanel>
          <div>
            <Box my={4}>
              <Typography variant="h4" align="center" color="primary">
                <strong>View Previous Statistics</strong>
              </Typography>
              <Typography align="center" color="primary">
                Click the buttons to update
              </Typography>
            </Box>
            <PrevStats />
          </div>
        </div>
      )}
    </Dashboard>
  )
}
