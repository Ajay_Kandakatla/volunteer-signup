import React, { useState, useEffect } from 'react'
import { Formik, Form } from 'formik'
import FormTextField from '../common/FormTextField'
import SelectField from '../common/SelectField'
import Card from '@material-ui/core/Card'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'
import { post } from '../../api/requests'
import Dashboard from './Dashboard'
import Options from '../Options'
import PhoneField from '../common/PhoneField'
import SSOField from '../common/SSOField'
import { useHistory } from 'react-router-dom'
import { validator } from '../../helpers/utilities'
import { FormHelperText } from '@material-ui/core'
import Alert from '@material-ui/lab/Alert'
import { removeDoubleQuotes } from '../../helpers/utilities'
/**
 * The page for creating issues
 */

const requiredFields = {
  name: true,
  sso: true,
  callBackNumber: true,
  // employmentTypes: true,
  location: true,
  // issueType: true,
  // spoc: true,
  // techOwner: true,
  issueDescription: true,
  // associateExternalEmail: true,
  // associateFunction: true,
  priority: true,
  intakeMethod: true
}
const useStyles = makeStyles((theme) => ({
  button: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    justifyContent: 'center',
    marginRight: theme.spacing(2)
  },
  root: {
    margin: 0,
    padding: theme.spacing(2)
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500]
  },
  formMargin: {
    marginLeft: theme.spacing(8),
    marginRight: theme.spacing(8),
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(4)
  }
}))

function AddExtension(values) {
  if (values.location == 'Hyderabad') {
    return '+91 ' + values.callBackNumber
  } else if (values.location == 'Manila' || values.location == 'Cebu') {
    return '+63 ' + values.callBackNumber
  } else {
    return '+1 ' + values.callBackNumber
  }
}

const errorTitle = 'Error Creating Issue'
const errorDesc = 'Please try again.'

const successTitle = 'Issue Successfully Created'
const successDesc = 'The issue can now be viewed on the View Issues page.'
const RequiredFieldText = () => (
  <React.Fragment>
    <h3>Info: You have not entered all the required Fields</h3>
    <ul>
      <li>Associate Name</li>
      <li>SSO</li>
      <li>Associate Call Back Number</li>
      <li>Location</li>
      <li>Issue Type</li>
      <li>Issue Description</li>
      <li>Priority</li>
      <li>Intake Method</li>
    </ul>
  </React.Fragment>
)
export default function CreateIssue() {
  // Note that we're using the 2nd version of Formik
  // so we're not going to use 1.x.x "render()"
  const classes = useStyles()
  let history = useHistory()

  const [open, setOpen] = useState(false)
  const [dialogTitle, setDialogTitle] = useState(successTitle)
  const [dialogDescription, setDialogDescription] = useState(successDesc)
  const [openError, setOpenError] = useState(false)
  const [formError, setFormError] = useState(false)
  const [conflictError, setConflictError] = useState(false)
  const [success, setSuccess] = useState(false)
  const [loading, setLoading] = useState(false)

  const handleClickOpen = (uploadSuccess) => {
    if (!uploadSuccess) {
      setDialogTitle(errorTitle)
      setDialogDescription(errorDesc)
    }
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
    // window.location.reload(false);
    // history.push("/edit");
  }

  // For the error form with ensuring phone # /SSO format
  const handleErrorClose = () => {
    setOpenError(false)
  }

  const options = Options()
  const submitForm = async (values, formikBag) => {
    await post('/api/issues', removeDoubleQuotes(values))
      .then((res) => res.json())
      .then((res) => {
        if (res.status == 'success') {
          {
            setSuccess(true)
            setLoading(false)
            window.scrollTo(0, 0)
            setFormError(false)
            formikBag.resetForm()
          }
        } else {
          //handleClickOpen(false);
        }
      })
      .catch((err) => {
        // setConflictError(true)
        return err
      })
  }
  return (
    <Dashboard currentPage="Add">
      <Card>
        <Formik
          initialValues={{
            // Populate with inital values here
            name: '',
            sso: '',
            callBackNumber: '',
            employmentTypes: '',
            location: '',
            issueType: '',
            spoc: '',
            techOwner: '',
            issueDescription: '',
            associateExternalEmail: '',
            associateFunction: '',
            priority: '',
            intakeMethod: '',
            nextCallbackTime: ''
          }}
          enableReinitialize={true}
          onSubmit={async (values, formikBag) => {
            // TODO (3/30/20): Add post request here -- lydia.tse@syf.com
            // Placeholder for now
            setLoading(true)
            setSuccess(false)
            setFormError(false)
            setConflictError(false)
            const hasFormError = validator(requiredFields, values)
            if (hasFormError) {
              setFormError(true)
              window.scrollTo(0, 0)
              setLoading(false)
              return
            } else {
              if (
                values.callBackNumber.length != 10 ||
                values.sso.length != 9
              ) {
                setOpenError(true)
                setLoading(false)
              } else {
                //values.callBackNumber = AddExtension(values);
                submitForm(values, formikBag)
              }
            }
          }}
        >
          <Form className={classes.formMargin}>
            <Typography variant="h4" component="h2" gutterBottom>
              Please fill out the form to create a new issue.
            </Typography>
            {formError && (
              <Alert severity="error" children={<RequiredFieldText />} />
            )}
            {success && (
              <Alert
                severity="success"
                children="Form submitted Successfully"
              />
            )}
            {conflictError && (
              <Alert severity="info" children={'Backend Failure'} />
            )}
            <FormTextField
              type="text"
              name="nextCallbackTime"
              label="Next Callback Time"
              required={false}
              variant="outlined"
            />
            <SelectField
              name="location"
              label="Location"
              options={options.locations}
              variant="outlined"
              required={true}
            />
            <SelectField
              name="priority"
              label="Priority"
              options={options.priorities}
              variant="outlined"
              required={true}
            />
            <FormTextField
              type="text"
              name="name"
              label="Associate Name"
              required={true}
              variant="outlined"
            />
            <SSOField
              name="sso"
              label="SSO"
              required={true}
              variant="outlined"
            />
            <PhoneField
              name="callBackNumber"
              label="Associate Call Back Number"
              required={true}
              variant="outlined"
            />
            <SelectField
              name="issueType"
              label="Issue Type"
              options={options.issueTypes}
              variant="outlined"
              required={true}
            />
            <FormTextField
              type="text"
              name="issueDescription"
              label="Issue Description"
              variant="outlined"
              required={true}
            />
            <FormTextField
              type="text"
              name="associateExternalEmail"
              label="Associate External Email"
              variant="outlined"
            />
            <FormTextField
              type="text"
              name="associateFunction"
              label="Associate Process/Function"
              variant="outlined"
            />
            <SelectField
              name="intakeMethod"
              label="Intake Method"
              options={options.intakeMethods}
              variant="outlined"
              required={true}
            />
            <SelectField
              name="employmentTypes"
              label="Employment Type"
              options={options.employmentTypes}
              variant="outlined"
            />
            <FormTextField
              type="text"
              name="spoc"
              label="Single Point of Contact (For IN/PH Only)"
              variant="outlined"
            />
            <FormTextField
              type="text"
              name="techOwner"
              label="Tech Owner (For IN/PH Only)"
              variant="outlined"
            />
            <Button
              className={classes.button}
              variant="contained"
              type="submit"
              disabled={loading}
              size="large"
              color="primary"
            >
              Submit
            </Button>
            <Button
              className={classes.button}
              variant="contained"
              disabled={loading}
              type="reset"
              size="large"
              color="secondary"
            >
              Clear
            </Button>
          </Form>
        </Formik>
      </Card>
      {/* <Button className={classes.button} variant="contained" onClick={handleClickOpen} size="large" color="primary">Open</Button> */}
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <DialogTitle id="customized-dialog-title">{dialogTitle}</DialogTitle>
        <DialogContent>
          <Typography gutterBottom>{dialogDescription}</Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            OK
          </Button>
        </DialogActions>
      </Dialog>

      <Dialog
        onClose={handleErrorClose}
        aria-labelledby="customized-dialog-title"
        open={openError}
      >
        <DialogTitle id="customized-dialog-title">
          Issue with SSO or Phone Number!
        </DialogTitle>
        <DialogContent>
          <Typography gutterBottom>
            Please ensure you used enough digits for the Phone Number and SSO.
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleErrorClose} color="primary">
            OK
          </Button>
        </DialogActions>
      </Dialog>
    </Dashboard>
  )
}
