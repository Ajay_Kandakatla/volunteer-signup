import React from "react";

/**
 * The page for default 404s
 */
export default function Error() {
  return (
    <h1>Sorry! The page you are looking for was not found.</h1>
  );
}
