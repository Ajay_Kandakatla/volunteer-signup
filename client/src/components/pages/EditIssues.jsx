import React, { useState, useEffect } from 'react'
import MUIDataTable from 'mui-datatables'
import { get } from '../../api/requests'
import Dashboard from './Dashboard'
import classnames from 'classnames'
import CustomToolbarSelect from '../common/CustomToolbarSelect'
import LinearProgress from '@material-ui/core/LinearProgress'
import {
  createMuiTheme,
  MuiThemeProvider,
  withStyles
} from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import { makeStyles } from '@material-ui/core/styles'
import Note from '../common/Note'
import Grid from '@material-ui/core/Grid'
import { colors } from './../../styles/theme'
import {
  countries,
  getObjectInfo,
  formatPhoneNumber,
  setRowProps
} from '../../helpers/utilities'
import Tooltip from '@material-ui/core/Tooltip'
import moment from 'moment'

const useStyles = makeStyles((theme) => ({
  button: {
    justifyContent: 'center',
    verticalAlign: 'middle'
  },
  margins: {
    marginBottom: theme.spacing(2),
    justifyContent: 'center'
  },
  note: {
    marginRight: theme.spacing(4),
    verticalAlign: 'middle'
  },
  cellText: {
    whiteSpace: 'pre',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    maxWidth: '250px',
    display: 'block'
  }
}))

const TextOnlyTooltip = withStyles({
  tooltip: {
    color: 'white',
    background: 'rgba(0, 0, 0, 0.85)',
    fontSize: '14px',
    lineHeight: '17px'
  }
})(Tooltip)
const updatedColumns = (columns, filterTypes, classes = '', data) => {
  const newColumns = columns.map((column, index) => {
    const timeStyle = {
      margin: 0,
      whiteSpace: 'nowrap'
    }
    column.options.filterList =
      columns.length === filterTypes.length
        ? filterTypes[index]
        : column.options.filterList
    column.options = {
      ...column.options,
      //custom cell render
      customBodyRender: (value, tableMeta, updateValue) => {
        const { rowIndex, tableData } = tableMeta

        if (column.name === 'Associate Call Back Number') {
          return (
            <p style={{ whiteSpace: 'nowrap' }}>{formatPhoneNumber(value)}</p>
          )
        } else if (column.name === 'Status') {
          const statusStyle = setRowProps(value)
          return (
            <React.Fragment>
              <p style={statusStyle}>{value}</p>
            </React.Fragment>
          )
        } else if (column.name === 'Last Updated') {
          return (
            <React.Fragment>
              <p style={timeStyle}>{moment(value).format('L')}</p>
              <p style={timeStyle}>{moment(value).format('LTS')}</p>
            </React.Fragment>
          )
        } else if (column.name === 'SSO') {
          const columnStyle = {
            backgroundColor:
              data && data[rowIndex].times > 1 ? '#FFD8D6' : 'white'
          }
          return <p style={columnStyle}>{value}</p>
        } else {
          return (
            <TextOnlyTooltip
              tooltipPlacementBottom
              classes={{ root: classes.toolTip }}
              title={value}
            >
              <span className={classes.cellText}>{value}</span>
            </TextOnlyTooltip>
          )
        }
      }
    }
    return column
  })
  return newColumns
}

export default function EditIssues(props) {
  const classes = useStyles()

  const [columns, setColumns] = useState([])
  const [data, setData] = useState([])
  const [loading, setLoading] = useState(true)
  const [filterTypes, setFilterTypes] = useState([])

  const {
    params: { countryId }
  } = props.match
  const current_page = 'Edit' + countryId
  const countryName = getObjectInfo(countries, 'id', countryId, 'name')

  const getTableData = () => {
    const {
      params: { countryId }
    } = props.match
    setLoading(true)
    get(`/api/issues/country/${countryId}`)
      .then((res) => res.json())
      .then((res) => {
        setColumns(updatedColumns(res.columns, filterTypes, classes, res.data))
        setData(res.data)
        setLoading(false)
      })
  }

  useEffect(() => {
    getTableData()
  }, [props.match])

  const options = {
    filterType: 'textField',
    responsive: 'scrollFullHeight',
    selectableRows: 'single',
    selectableRowsOnClick: true,
    fixedHeaderOptions: {
      xAxis: false,
      yAxis: true
    },
    onFilterChange: (column, filterList) => {
      setFilterTypes(filterList)
    },
    rowsPerPage: 50,
    rowsPerPageOptions: [10, 25, 50, 100],
    customToolbarSelect: (selectedRows) => {
      return (
        <CustomToolbarSelect
          selectedRows={selectedRows}
          data={data}
          refresh={() => getTableData()}
        />
      )
    }
  }

  const getMuiTheme = () =>
    createMuiTheme({
      palette: {
        primary: {
          main: colors.charcoalGray,
          contrastText: colors.white
        },
        secondary: {
          main: colors.syfGold,
          contrastText: colors.black
        },
        backgroundDefault: {
          main: colors.white,
          contrastText: colors.black
        }
      },
      overrides: {
        MuiToolbar: {
          root: {
            position: 'sticky',
            top: '64px',
            zIndex: '302',
            background: 'inherit'
          }
        },
        MUIDataTableFilterList: {
          root: {
            position: 'sticky',
            top: '128px',
            zIndex: '301',
            background: 'inherit'
          }
        },
        MUIDataTableToolbarSelect: {
          root: {
            justifyContent: 'start',
            position: 'sticky',
            top: '64px',
            zIndex: '302'
          },
          button: {
            backgroundColor: colors.white
          }
        },
        MUIDataTableHeadCell: {
          fixedHeaderYAxis: {
            position: 'sticky',
            top: '168px',
            zIndex: '300'
          }
        },
        MUIDataTableSelectCell: {
          fixedHeaderCommon: {
            position: 'relative'
          }
        },
        MuiButton: {
          containedSecondary: {
            backgroundColor: colors.syfGold,
            color: colors.black
          },
          containedPrimary: {
            backgroundColor: colors.charcoalGray,
            color: colors.white
          }
        },
        MUIDataTableBodyCell: {
          root: {
            padding: '0px 4px 0px 4px',
            borderRight: '0.5px solid lightgrey'
          }
        }
      }
    })

  return (
    <Dashboard currentPage={current_page}>
      <Grid container className={classes.margins}>
        <Grid item>
          <Button
            className={classes.button}
            onClick={getTableData}
            color="secondary"
            variant="contained"
            size="large"
          >
            REFRESH TABLE
          </Button>
        </Grid>
      </Grid>
      {loading ? (
        <LinearProgress />
      ) : (
        <>
          <MuiThemeProvider theme={getMuiTheme()}>
            <MUIDataTable
              title={`Current WFH ${countryName} Issues`}
              columns={columns}
              data={data}
              options={options}
            />
          </MuiThemeProvider>
        </>
      )}
    </Dashboard>
  )
}
