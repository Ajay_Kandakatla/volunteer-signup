import React, { useState, useEffect } from 'react'
import Dashboard from './Dashboard'
import { makeStyles } from '@material-ui/core/styles'
import Link from '@material-ui/core/Link'
import Typography from '@material-ui/core/Typography'
import { Button } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center',
    flexWrap: 'wrap',
    '& > a': {
      margin: '10px',
      width: '267px !important'
    },
    '& > * + *': {
      marginLeft: theme.spacing(2)
    }
  }
}))

const style = { display: 'inline-block' }

export default function Resources() {
  const classes = useStyles()

  return (
    <Dashboard currentPage="Resources">
      <div className={classes.root}>
        <Button variant="contained" color="primary" size="large" href="/edit/1">
          Edit Issue - US
        </Button>
        <Button
          variant="contained"
          color="primary"
          size="large"
          href="https://intranet.syfbank.com/Engage/OurFunctions/EnterpriseOperations/WAH/Pages/default.aspx"
          target="_blank"
        >
          EO Work at Home
        </Button>

        <Button
          variant="contained"
          color="primary"
          size="large"
          href="https://teamsites.syfbank.com/sites/InfraPM/_layouts/15/WopiFrame2.aspx?sourcedoc=%7b2a8dc689-ecb4-4762-ad8b-ce4577efae64%7d&action=view&activeCell=%27TECHNICIANS%27!E16&wdInitialSession=9c42429f-7051-1000-fc0c-3bcde0c0d851&wdRldC=1"
          target="_blank"
        >
          Coordinator Schedule
        </Button>

        <Button
          variant="contained"
          color="secondary"
          size="large"
          href="https://teams.microsoft.com/l/channel/19%3A8026983b2edc4f088d61350cb3f293a2%40thread.tacv2/tab%3A%3A03086ab6-2745-4798-97da-ce2f34ea3ee6?groupId=ec1b1e24-6151-44f9-8b95-e5e5198ceef5&tenantId=05e91730-bfa3-405b-b814-bd9b0ee89317"
          target="_blank"
        >
          Start My Day U.S.
        </Button>
        <Button
          variant="contained"
          color="secondary"
          size="large"
          href="https://teams.microsoft.com/l/channel/19%3A8026983b2edc4f088d61350cb3f293a2%40thread.tacv2/tab%3A%3A03086ab6-2745-4798-97da-ce2f34ea3ee6?groupId=ec1b1e24-6151-44f9-8b95-e5e5198ceef5&tenantId=05e91730-bfa3-405b-b814-bd9b0ee89317"
          target="_blank"
        >
          Start My Day India
        </Button>
        <Button
          variant="contained"
          color="secondary"
          size="large"
          href="https://teams.microsoft.com/l/channel/19%3A8026983b2edc4f088d61350cb3f293a2%40thread.tacv2/tab%3A%3A03086ab6-2745-4798-97da-ce2f34ea3ee6?groupId=ec1b1e24-6151-44f9-8b95-e5e5198ceef5&tenantId=05e91730-bfa3-405b-b814-bd9b0ee89317"
          target="_blank"
        >
          Start My Day Philippines
        </Button>
      </div>
    </Dashboard>
  )
}
