import React from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogTitle from "@material-ui/core/DialogTitle";
import ConfirmButton from "../buttons/ConfirmButton";
import CancelButton from "../buttons/CancelButton";

export default function StatusDialog({
  open,
  status,
  handleClose,
  refresh,
  data
}) {
  return (
    <Dialog open={open}>
      <DialogTitle id="form-dialog-title">Mark as {status}?</DialogTitle>
      <DialogActions>
        <CancelButton handleClose={handleClose} />
        <ConfirmButton
          status={status}
          handleClose={handleClose}
          refresh={refresh}
          id={data.Id}
        />
      </DialogActions>
    </Dialog>
  );
}
