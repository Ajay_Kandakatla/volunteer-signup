import React, { useEffect, useState } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogTitle from "@material-ui/core/DialogTitle";
import ConfirmButton from "../buttons/ConfirmButton";
import CancelButton from "../buttons/CancelButton";
import { get } from "../../../api/requests";
import { DialogContent } from "@material-ui/core";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import CircularProgress from "@material-ui/core/CircularProgress";
import { makeStyles } from '@material-ui/core/styles';
import Alert from '@material-ui/lab/Alert';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  }
}));

export default function RevertDialog({ open, data, handleClose, refresh }) {
  const classes = useStyles();

  const [options, setOptions] = useState([]);
  const [status, setStatus] = useState('');
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);

  const handleChange = (e) => {
    setStatus(e.target.value);
    setError(false);
  };

  useEffect(() => {
    if (open) {
      get("/api/form-options")
        .then((res) => res.json())
        .then((res) => {
          setOptions(res.statuses);
        });
    }
  }, [open]);

  useEffect(() => {
    if (options.length > 0) {
      setLoading(false);
    }
  }, [options]);

  return (
    <Dialog open={open} onClose={handleClose}>
      {loading ? (
        <CircularProgress />
      ) : (
        <>
          <DialogTitle>Revert Status To:</DialogTitle>
          <DialogContent>
            <FormControl required className={classes.formControl}>
              <InputLabel>Status</InputLabel>
              <Select 
                value={status}
                onChange={handleChange}
                className={classes.selectEmpty}
              >
                {options.map((option) => (
                  <MenuItem key={option} value={option}>
                    {option}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
            {error &&
              <Alert severity="error">Please select a Status!</Alert>
            }
          </DialogContent>
          <DialogActions>
            <CancelButton handleClose={handleClose} />
            <ConfirmButton
              status={status}
              id={data.Id}
              handleClose={handleClose}
              refresh={refresh}
              error={() => setError(true)}
            />
          </DialogActions>
        </>
      )}
    </Dialog>
  );
}
