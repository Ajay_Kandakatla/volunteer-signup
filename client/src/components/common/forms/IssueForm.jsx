import React, { useState, useEffect } from 'react'
import { Formik, Form, Field } from 'formik'
import FormTextField from '../FormTextField'
import SelectField from '../SelectField'
import Card from '@material-ui/core/Card'
import Button from '@material-ui/core/Button'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import { put } from '../../../api/requests'
import PhoneField from '../PhoneField'
import SSOField from '../SSOField'
import Alert from '@material-ui/lab/Alert'
import { removeDoubleQuotes } from '../../../helpers/utilities'
/**
 * The page for creating issues
 */

const useStyles = makeStyles((theme) => ({
  button: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    justifyContent: 'center'
  },
  root: {
    margin: 0,
    padding: theme.spacing(2)
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500]
  },
  formMargin: {
    marginLeft: theme.spacing(8),
    marginRight: theme.spacing(8),
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(4)
  }
}))

function RemoveExtension(phone_number) {
  if (phone_number.length == 10 || phone_number.length < 10) {
    return phone_number
  } else {
    const number_to_remove = phone_number.length - 10
    return phone_number.slice(number_to_remove)
  }
}
function AddExtension(values) {
  if (values.location == 'Hyderabad') {
    return '+91 ' + values.callBackNumber
  } else if (values.location == 'Manila' || values.location == 'Cebu') {
    return '+63 ' + values.callBackNumber
  } else {
    return '+1 ' + values.callBackNumber
  }
}

export default function IssueForm({
  data,
  info,
  selectOptions,
  handleClose,
  refresh,
  formId
}) {
  // Note that we're using the 2nd version of Formik
  // so we're not going to use 1.x.x "render()"
  const classes = useStyles()
  const [loading, setLoading] = useState(true)
  const [conflictError, setConflictError] = useState(false)
  // let alertRef;
  useEffect(() => {
    if (info && selectOptions) {
      setLoading(false)
    }
  }, [info])

  var phone_number = info.phone
  if (phone_number != 'undefined') {
    phone_number = RemoveExtension(info.phone)
  }

  return (
    <Card>
      <Formik
        enableReinitialize
        initialValues={{
          assignee: info.assignee,
          notes: info.notes,
          status: info.status,
          name: info.name,
          sso: Number(info.sso),
          callBackNumber: Number(phone_number),
          location: info.location,
          issueType: info.issueType,
          issueDescription: info.description,
          associateExternalEmail: info.email,
          associateFunction: info.function,
          priority: info.priority,
          intakeMethod: info.intake,
          nextCallbackTime: info.nextCallbackTime
        }}
        onSubmit={async (values) => {
          values.prevStatus = info.prevStatus
          setConflictError(false)
          put(`/api/issues/${data.Id}`, removeDoubleQuotes(values))
            .then((res) => res.json())
            .then((res) => {
              if (res.status === 'success') {
                handleClose()
                if (refresh) {
                  refresh()
                }
              } else {
                alert('failed to update')
              }
            })
            .catch((error) => {
              setConflictError(true)
            })
        }}
      >
        <Form className={classes.formMargin} id={formId}>
          <SelectField
            name="status"
            label="Status"
            options={selectOptions.statuses}
            required={false}
            variant="filled"
          />
          <FormTextField
            type="text"
            name="assignee"
            label="Assignee Name"
            required={false}
            variant="filled"
          />
          <FormTextField
            type="text"
            name="notes"
            label="Notes"
            // rows="4"
            required={false}
            variant="filled"
          />
          <FormTextField
            type="text"
            name="nextCallbackTime"
            label="Next Callback Time"
            required={false}
            variant="outlined"
          />
          <SelectField
            name="location"
            label="Location"
            options={selectOptions.locations}
            variant="outlined"
          />
          <SelectField
            name="priority"
            label="Priority"
            options={selectOptions.priorities}
            required={true}
            variant="outlined"
          />
          <FormTextField
            type="text"
            name="name"
            label="Associate Name"
            required={true}
            variant="outlined"
          />
          <SSOField name="sso" label="SSO" required={true} variant="outlined" />
          <PhoneField
            name="callBackNumber"
            label="Associate Call Back Number"
            required={true}
            variant="outlined"
          />
          <SelectField
            name="issueType"
            label="Issue Type"
            options={selectOptions.issueTypes}
            variant="outlined"
          />
          <FormTextField
            type="text"
            name="issueDescription"
            label="Issue Description"
            required={true}
            variant="outlined"
          />
          <FormTextField
            type="text"
            name="associateExternalEmail"
            label="Associate External Email"
            required={false}
            variant="outlined"
          />
          <FormTextField
            type="text"
            name="associateFunction"
            label="Associate Process/Function"
            required={false}
            variant="outlined"
          />
          <SelectField
            name="intakeMethod"
            label="Intake Method"
            options={selectOptions.intakeMethods}
            required={false}
            variant="outlined"
          />
          <SelectField
            name="employmentTypes"
            label="Employement Type"
            options={selectOptions.employmentTypes}
            variant="outlined"
          />
          <FormTextField
            type="text"
            name="spoc"
            label="Single Point of Contact (For IN/PH Only)"
            variant="outlined"
          />
          <FormTextField
            type="text"
            name="techOwner"
            label="Tech Owner (For IN/PH Only)"
            variant="outlined"
          />
          {conflictError && (
            <Alert severity="error" children="Backend Failure" />
          )}
        </Form>
      </Formik>
    </Card>
  )
}
