import React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { Formik } from "formik";
import { put } from "../../../api/requests";

// The page for editing notes
export default function NotesFormDialog({ open, handleClose, data, refresh }) {
  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Edit Notes:</DialogTitle>
        <Formik
          initialValues={{
            notes: data.Notes ? data.Notes : ""
          }}
          onSubmit={(values) => {
            put("/api/issues/notes/" + data.Id, values)
              .then((res) => res.json())
              .then((res) => {
                if (res.status === "success") {
                  handleClose();
                  refresh();
                }
              });
          }}
        >
          {({ values, handleChange, handleSubmit }) => (
            <form onSubmit={handleSubmit}>
              <DialogContent>
                <TextField
                  name="notes"
                  label="Notes"
                  multiline
                  rows="4"
                  required={true}
                  value={values.notes}
                  variant="outlined"
                  onChange={handleChange}
                />
              </DialogContent>
              <DialogActions>
                <Button
                  onClick={handleClose}
                  color="secondary"
                  variant="outlined"
                >
                  Cancel
                </Button>
                <Button type="submit" color="primary" variant="contained">
                  Submit
                </Button>
              </DialogActions>
            </form>
          )}
        </Formik>
      </Dialog>
    </div>
  );
}
