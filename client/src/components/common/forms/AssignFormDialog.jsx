import React, { useState, useEffect } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { Formik, Form, Field, useFormikContext } from "formik";
import { put } from "../../../api/requests";

export default function AssignFormDialog({ open, handleClose, data, refresh }) {
  const handleSubmit = (values) => {
    put(`/api/issues/assignee/${data.Id}`, values)
      .then((res) => res.json())
      .then((res) => {
        if (res.status === "success") {
          handleClose();
          refresh();
        }
      });
  };
  return (
    <Formik
      initialValues={{
        name: ""
      }}
      onSubmit={handleSubmit}
    >
      {({ values, handleChange, handleSubmit }) => {
        return (
          <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="form-dialog-title"
          >
            <DialogTitle id="form-dialog-title">Assign Issue</DialogTitle>
            <DialogContent>
              <Field
                name="name"
                id="name"
                label="Assignee Name"
                onChange={handleChange}
                component={TextField}
                fullWidth
                margin="normal"
                value={values["name"]}
              />
            </DialogContent>
            <DialogActions>
              <Button
                onClick={handleClose}
                color="secondary"
                variant="outlined"
              >
                Cancel
              </Button>
              <Button
                onClick={handleSubmit}
                variant="contained"
                color="primary"
              >
                Submit
              </Button>
            </DialogActions>
          </Dialog>
        );
      }}
    </Formik>
  );
}
