import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogTitle from "@material-ui/core/DialogTitle";
import { put } from "../../../api/requests";

export default function FirstAttemptFormDialog({
  open,
  data,
  handleClose,
  refresh
}) {
  const handleConfirm = () => {
    put(`/api/issues/status/${data.Id}`, {
      status: "First Attempt"
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.status === "success") {
          handleClose();
          refresh();
        }
      });
  };
  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">
          Confirm First Attempt at Reaching Out?
        </DialogTitle>
        <DialogActions>
          <Button onClick={handleClose} color="secondary" variant="outlined">
            Cancel
          </Button>
          <Button onClick={handleConfirm} color="primary" variant="contained">
            Confirm
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
