import React, { useState, useEffect } from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import IssueForm from './IssueForm'
import LinearProgress from '@material-ui/core/LinearProgress'
import { get } from '../../../api/requests'

export default function EditFormDialog({ open, handleClose, data, refresh }) {
  const [info, setInfo] = useState({})
  const [loading, setLoading] = useState(true)
  const [options, setOptions] = useState([])
  const [selectOptions, setSelectOptions] = useState({})
  const formId = 'issueForm'

  // Max Mitchell - might want to refactor this to use data already in the table to prevent extra API call
  useEffect(() => {
    if (open) {
      get(`/api/issues/${data.Id}`)
        .then((res) => res.json())
        .then((res) => {
          res.prevStatus = res.status
          setInfo(res)
        })
      get('/api/form-options')
        .then((res) => res.json())
        .then((res) => {
          setOptions(res)
          setSelectOptions(res)
          setLoading(false)
        })
    }
  }, [open])

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Edit Issue</DialogTitle>
        <DialogContent>
          {loading ? (
            <LinearProgress />
          ) : (
            <IssueForm
              options={options}
              handleClose={handleClose}
              data={data}
              info={info}
              refresh={refresh}
              selectOptions={selectOptions}
              formId={formId}
            />
          )}
        </DialogContent>
        <DialogActions>
          <Button
            form={formId}
            disabled={loading}
            type="submit"
            value="Submit"
            color="primary"
            variant="outlined"
          >
            Submit
          </Button>

          <Button onClick={handleClose} color="secondary" variant="outlined">
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}
