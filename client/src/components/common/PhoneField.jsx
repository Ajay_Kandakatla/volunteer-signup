import React, { useState, useEffect } from "react";
import NumberFormat from 'react-number-format';
import { Field, useFormikContext } from "formik";
import TextField from "@material-ui/core/TextField";

//https://material-ui.com/components/text-fields/
//http://s-yadav.github.io/react-number-format/

function NumberFormatCustom(props) {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={(values) => {
        onChange({
          target: {
            value: values.value,
          },
        });
      }}
      isNumericString
      format="###-###-####"
      placeholder="###-###-####"
      mask="#"
    />
  );
}
export default function PhoneField({ type, label, name, required }) {
  let placeholder = "Enter " + label;
  const { values, handleChange } = useFormikContext();
  return (
  <Field
    type={type}
    name={name}
    id={name}
    label={label}
    component={TextField}
    variant="outlined"
    placeholder={placeholder}
    fullWidth
    required={required}
    margin="normal"
    onChange={handleChange(name)}
    value={values[name]}
    InputProps={{
      inputComponent: NumberFormatCustom,
      minLength: 10
    }}
  />
);
}
