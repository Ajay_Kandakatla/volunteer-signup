import React from 'react'
import {
  BarChart,
  Bar,
  Cell,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  Label
} from 'recharts'

export default function StatsBarChart({
  data,
  xAxisKey,
  barDataKeys,
  barDataLabels,
  colors
}) {
  console.log(barDataLabels, colors)
  return (
    <BarChart
      width={800}
      height={500}
      data={data}
      barCategoryGap={10}
      margin={{ top: 5, right: 30, left: 20, bottom: 30 }}
    >
      <Legend align="right" verticalAlign="top" />
      <XAxis dataKey={xAxisKey}>
        <Label offset={0} value="Time" position="bottom" />
      </XAxis>
      <YAxis>
        <Label angle={-90} value="Number of Issues Opened" position="left" />
      </YAxis>
      <Tooltip
        labelStyle={{ color: 'black' }}
        contentStyle={{ color: 'black' }}
      />
      {barDataKeys.map((key, index) => (
        <Bar
          key={index}
          dataKey={key}
          name={barDataLabels[index]}
          fill={colors[index]}
        />
      ))}
    </BarChart>
  )
}
