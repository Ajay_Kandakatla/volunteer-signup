import React from "react";
import { Field, useFormikContext } from "formik";
import TextField from "@material-ui/core/TextField";

export default function FormTextField({ type, label, name, required, variant }) {
  let placeholder = "Enter " + label;
  const { values, handleChange } = useFormikContext();
  return (
  <Field
    type={type}
    name={name}
    id={name}
    label={label}
    component={TextField}
    variant={variant}
    placeholder={placeholder}
    fullWidth
    multiline={true}
    required={required}
    margin="normal"
    onChange={handleChange(name)}
    value={values[name]}
  />
);
}
