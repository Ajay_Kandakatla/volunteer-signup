import React, { useState } from 'react'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import Select from '@material-ui/core/Select'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import MenuItem from '@material-ui/core/MenuItem'
import DayTextField from './DayTextField'
import SearchIcon from '@material-ui/icons/Search'
import { Button, Grid } from '@material-ui/core'
import StatsSection from '../common/StatsSection'
import { get } from '../../api/requests'
import ExpansionPanel from '@material-ui/core/ExpansionPanel'
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import Typography from '@material-ui/core/Typography'
import LinearProgress from '@material-ui/core/LinearProgress'
import Box from '@material-ui/core/Box'
import {
  countries,
  getObjectInfo,
  employeeTypes
} from '../../helpers/utilities'

const useStyles = makeStyles((theme) => ({
  margin: {
    margin: theme.spacing(1)
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 200
  },
  iconButton: {
    marginLeft: theme.spacing(1),
    marginTop: theme.spacing(2)
  },
  centered: {
    margin: 'auto',
    alignContent: 'center'
  }
}))

export default function PrevStats(props) {
  const classes = useStyles()
  const [numDays, setNumDays] = useState(3)
  const [data, setData] = useState({})
  const [showData, setShowData] = useState(false)
  const [date, setDate] = useState('')
  const [countryId, setCountryId] = useState(0)
  const [employeeType, setEmployeeType] = useState(0)
  const [loading, setLoading] = useState(true)
  const [needsBarChart, setNeedsBarChart] = useState(false)

  const handleChangeDays = (event) => {
    setNumDays(event.target.value)
  }

  const handleChangeDate = (event) => {
    setDate(event.target.value)
  }

  const handleChangeCountry = (event) => {
    setCountryId(event.target.value)
  }

  const handleEmployeeTypeChange = (event) => {
    setEmployeeType(event.target.value)
  }

  // TODO: change api url
  const searchPastDays = () => {
    const countryValue = getObjectInfo(countries, 'id', countryId, 'value')
    const employeeTypeValue = getObjectInfo(
      employeeTypes,
      'id',
      employeeType,
      'value'
    )
    get(
      `/api/issues/stats/values?day=${numDays}&country=${countryValue}&employeeType=${employeeTypeValue}`
    )
      .then((res) => res.json())
      .then((res) => {
        setData(res)
        setNeedsBarChart(false)
        setShowData(true)
        setLoading(false)
      })
  }

  const searchDate = () => {
    const countryValue = getObjectInfo(countries, 'id', countryId, 'value')
    const employeeTypeValue = getObjectInfo(
      employeeTypes,
      'id',
      employeeType,
      'value'
    )
    console.log('DATE', date)
    get(
      `/api/issues/stats/values?date=${date}&country=${countryValue}&employeeType=${employeeTypeValue}`
    )
      .then((res) => res.json())
      .then((res) => {
        setData(res)
        setNeedsBarChart(true)
        setShowData(true)
        setLoading(false)
      })
  }

  return (
    <div>
      <Grid container justify="center">
        <Grid item>
          <FormControl
            onSubmit={(e) => {
              e.preventDefault()
            }}
            variant="outlined"
            className={classes.formControl}
          >
            <InputLabel htmlFor="country-select">Country</InputLabel>
            <Select
              label="Country"
              value={countryId}
              onChange={handleChangeCountry}
              inputProps={{ name: 'country', id: 'country-select' }}
            >
              {countries.map((country) => {
                if (country.name == 'All')
                  return (
                    <MenuItem value={country.id} selected>
                      {country.name}
                    </MenuItem>
                  )
                else
                  return <MenuItem value={country.id}>{country.name}</MenuItem>
              })}
            </Select>
          </FormControl>
        </Grid>
        <Grid item>
          <FormControl variant="outlined" className={classes.formControl}>
            <InputLabel htmlFor="employee-type-input">Employee Type</InputLabel>
            <Select
              label="Employee Type"
              value={employeeType}
              onChange={handleEmployeeTypeChange}
              inputProps={{
                name: 'employeeType',
                id: 'employee-type-input'
              }}
            >
              {employeeTypes.map((employeeType) => {
                if (employeeType.name == 'All')
                  return (
                    <MenuItem value={employeeType.id} selected>
                      {employeeType.name}
                    </MenuItem>
                  )
                else
                  return (
                    <MenuItem value={employeeType.id}>
                      {employeeType.name}
                    </MenuItem>
                  )
              })}
            </Select>
          </FormControl>
        </Grid>
      </Grid>
      <Grid container justify="center">
        <Grid item>
          <Box fontWeight="fontWeightBold">
            <Typography variant="h6">AND</Typography>
          </Box>
        </Grid>
      </Grid>
      <Grid container justify="center">
        <Grid item>
          <FormControl
            onSubmit={(e) => {
              e.preventDefault()
            }}
            variant="outlined"
            className={classes.formControl}
          >
            <InputLabel htmlFor="prev-days-input">Previous Days</InputLabel>
            <Select
              label="Previous Days"
              value={numDays}
              onChange={handleChangeDays}
              inputProps={{
                name: 'numDays',
                id: 'prev-days-input'
              }}
            >
              <MenuItem value={1}>1</MenuItem>
              <MenuItem value={3} selected>
                3
              </MenuItem>
              <MenuItem value={5}>5</MenuItem>
              <MenuItem value={7}>7</MenuItem>
            </Select>
          </FormControl>
        </Grid>
        <Grid>
          <Button
            variant="contained"
            color="default"
            className={classes.iconButton}
            endIcon={<SearchIcon />}
            onClick={searchPastDays}
            size="medium"
          >
            Search Last {numDays} Days
          </Button>
        </Grid>
        <Grid item>
          <Box fontWeight="fontWeightBold" pt={2} pl={1} pr={1}>
            <Typography variant="h6">OR</Typography>
          </Box>
        </Grid>
        <Grid item>
          <DayTextField handleChange={handleChangeDate} />
        </Grid>
        <Grid item>
          <Button
            variant="contained"
            color="default"
            className={classes.iconButton}
            endIcon={<SearchIcon />}
            onClick={searchDate}
            size="medium"
          >
            Search by Date
          </Button>
        </Grid>
      </Grid>
      {data && showData && (
        <>
          {loading ? (
            <LinearProgress color="secondary" />
          ) : (
            <ExpansionPanel>
              <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <h1 align="center">Search Results</h1>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <StatsSection
                  data={data}
                  className={classes.margin}
                  includesBarChart={needsBarChart}
                />
              </ExpansionPanelDetails>
            </ExpansionPanel>
          )}
        </>
      )}
    </div>
  )
}
