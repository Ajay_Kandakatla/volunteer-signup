import React from "react";
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Card from "@material-ui/core/Card";
import Box from '@material-ui/core/Box';

const theme = createMuiTheme({
    typography:{
        subtitle1: {
            fontSize: 18,
            fontFamily: 'Roboto',
            fontWeight: "fontWeightBold",
          },
    },
});

export default function Note(props) {
    return(
        <Card>
            <ThemeProvider theme={theme}>
            <Typography variant="h6" gutterBottom>
                {props.content.map((note, index) => (
                   <Box key={index} m={1} color={props.color}>
                        {note}
                    </Box>
                ))}
            </Typography>
            </ThemeProvider>
      </Card>
    );

}