import React, { useState, useEffect } from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { colors } from "../../styles/theme";

const StyledTableHeaderCell = withStyles((theme) => ({
  head: {
    backgroundColor: colors.steelGray,
    color: colors.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableCell = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: colors.syfGold,
    },
  },
}))(TableCell);

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
  tableContainer: {
    display: "inline-block",
    minWidth: "400",
  },
}));

const percentageFormat = (value) => value.toFixed(2);

export default function KeyStatsTable(props) {
  const classes = useStyles();

  return (
    <TableContainer component={Paper} className={classes.tableContainer}>
      <Table size="small" aria-label="a dense table" display="inline-block">
        <TableHead>
          <TableRow>
            {props.columns.map((column, index) => (
              <StyledTableHeaderCell
                align={index > 0 ? "right" : "inherit"}
                key={index}
              >
                {column}
              </StyledTableHeaderCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {props.rows.map((row) => (
            <TableRow key={row.name}>
              <StyledTableCell component="th" scope="row">
                {row.name}
              </StyledTableCell>
              <TableCell className={classes.tableCell} align="right">
                {row.count}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
