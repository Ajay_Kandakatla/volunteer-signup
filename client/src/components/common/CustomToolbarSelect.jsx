import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import EditIcon from "@material-ui/icons/Edit";
import AssignmentIndIcon from "@material-ui/icons/AssignmentInd";
import AssignmentTurnedInIcon from "@material-ui/icons/AssignmentTurnedIn";
import NotesIcon from "@material-ui/icons/Notes";
import ReplayIcon from "@material-ui/icons/Replay";
import AutorenewIcon from "@material-ui/icons/Autorenew";
import { Button } from "@material-ui/core";
import EditFormDialog from "./forms/EditFormDialog";
import AssignFormDialog from "./forms/AssignFormDialog";
import NotesFormDialog from "./forms/NotesFormDialog";
import CompleteFormDialog from "./forms/CompleteFormDialog";
import FirstAttemptFormDialog from "./forms/FirstAttemptFormDialog";
import SecondAttemptFormDialog from "./forms/SecondAttemptFormDialog";
import StatusDialog from "./dialogs/StatusDialog";
import CancelScheduleSendIcon from "@material-ui/icons/CancelScheduleSend";
import RevertDialog from "./dialogs/RevertDialog";

const useStyles = makeStyles((theme) => ({
  iconButton: {
    marginLeft: theme.spacing(1)
  }
}));

export default function CustomToolbarSelect(props) {
  const [editOpen, setEditOpen] = React.useState(false);
  const [assignOpen, setAssignOpen] = React.useState(false);
  const [completeOpen, setCompleteOpen] = React.useState(false);
  const [notesOpen, setNotesOpen] = React.useState(false);
  const [firstAttemptOpen, setFirstAttemptOpen] = React.useState(false);
  const [secondAttemptOpen, setSecondAttemptOpen] = React.useState(false);
  const [noResponseOpen, setNoResponseOpen] = React.useState(false);
  const [revertOpen, setRevertOpen] = React.useState(false);

  const classes = useStyles();

  const curRowData = props.data[props.selectedRows.data[0].dataIndex];
  const curRowStatus = curRowData.Status;

  // Edit Form
  const handleEditClick = () => {
    setEditOpen(true);
  };

  // Assign Form
  const handleAssignClick = () => {
    setAssignOpen(true);
  };

  // First Attempt Form
  const handleFirstAttemptClick = () => {
    setFirstAttemptOpen(true);
  };

  // Second Attempt Form
  const handleSecondAttemptClick = () => {
    setSecondAttemptOpen(true);
  };

  // No Response Form
  const handleNoResponseClick = () => {
    setNoResponseOpen(true);
  };

  // Complete Form
  const handleCompleteClick = () => {
    setCompleteOpen(true);
  };

  // Notes Form
  const handleNotesClick = () => {
    setNotesOpen(true);
  };

  const handleRevertClick = () => {
    setRevertOpen(true);
  };

  return (
    <div className={classes.buttonAllign}>
      <Button
        variant="contained"
        color="secondary"
        className={classes.iconButton}
        endIcon={<EditIcon />}
        onClick={handleEditClick}
      >
        Edit Issue
      </Button>
      {/* <Button
        variant="contained"
        color="secondary"
        className={classes.iconButton}
        endIcon={<AssignmentIndIcon />}
        onClick={handleAssignClick}
      >
        Assign Issue
      </Button>
      <Button
        variant="contained"
        color="secondary"
        className={classes.iconButton}
        endIcon={<NotesIcon />}
        onClick={handleNotesClick}
      >
        Edit Notes
      </Button> */}
      {curRowStatus.toLowerCase() === "in progress" && (
        <Button
          variant="contained"
          color="primary"
          className={classes.iconButton}
          endIcon={<ReplayIcon />}
          onClick={handleFirstAttemptClick}
        >
          First Attempt
        </Button>
      )}
      {curRowStatus.toLowerCase() === "first attempt" && (
        <Button
          variant="contained"
          color="primary"
          className={classes.iconButton}
          endIcon={<AutorenewIcon />}
          onClick={handleSecondAttemptClick}
        >
          Second Attempt
        </Button>
      )}
      {curRowStatus.toLowerCase() === "second attempt" && (
        <Button
          variant="contained"
          color="secondary"
          className={classes.iconButton}
          endIcon={<CancelScheduleSendIcon />}
          onClick={handleNoResponseClick}
        >
          No Response
        </Button>
      )}
      {curRowStatus.toLowerCase() !== "unassigned" && (
          <Button
            variant="contained"
            color="primary"
            className={classes.iconButton}
            endIcon={<AssignmentTurnedInIcon />}
            onClick={handleRevertClick}
          >
            Revert Status
          </Button>
      )}
      {curRowStatus.toLowerCase() !== "unassigned" && curRowStatus.toLowerCase() !== "complete" && (
          <Button
            variant="contained"
            color="primary"
            className={classes.iconButton}
            endIcon={<AssignmentTurnedInIcon />}
            onClick={handleCompleteClick}
          >
            Mark Complete
          </Button>
      )}

      <EditFormDialog
        open={editOpen}
        handleClose={() => setEditOpen(false)}
        data={curRowData}
        refresh={props.refresh}
      />

      <AssignFormDialog
        open={assignOpen}
        handleClose={() => setAssignOpen(false)}
        data={curRowData}
        refresh={props.refresh}
      />

      <NotesFormDialog
        open={notesOpen}
        handleClose={() => setNotesOpen(false)}
        data={curRowData}
        refresh={props.refresh}
      />

      <StatusDialog
        open={noResponseOpen}
        status="No Response"
        handleClose={() => setNoResponseOpen(false)}
        data={curRowData}
        refresh={props.refresh}
      />

      <StatusDialog
        open={completeOpen}
        status="Complete"
        handleClose={() => setCompleteOpen(false)}
        data={curRowData}
        refresh={props.refresh}
      />

      <FirstAttemptFormDialog
        open={firstAttemptOpen}
        handleClose={() => setFirstAttemptOpen(false)}
        data={curRowData}
        refresh={props.refresh}
      />

      <SecondAttemptFormDialog
        open={secondAttemptOpen}
        handleClose={() => setSecondAttemptOpen(false)}
        data={curRowData}
        refresh={props.refresh}
      />

      <RevertDialog
        open={revertOpen}
        data={curRowData}
        handleClose={() => setRevertOpen(false)}
        refresh={props.refresh}
      />
    </div>
  );
}
