import React, { useState, useEffect } from 'react'
import Grid from '@material-ui/core/Grid'
import StatsTable from '../common/StatsTable'
import { makeStyles } from '@material-ui/core/styles'
import LinearProgress from '@material-ui/core/LinearProgress'
import StatsBarChart from './charts/StatsBarChart'
import Typography from '@material-ui/core/Typography'
import { colors } from '../../styles/theme'
import Container from '@material-ui/core/Container'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    display: 'inline-block'
  }
}))

export default function StatsSection({ data, className, includesBarChart }) {
  const classes = useStyles()
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    if (data) {
      setLoading(false)
    } else {
      setLoading(true)
    }
  }, [data])

  return (
    <div>
      {loading ? (
        <LinearProgress />
      ) : (
        <Grid
          container
          direction="column"
          alignItems="center"
          justify="center"
          spacing={2}
        >
          {includesBarChart && (
            <Grid item>
              <Typography>Number of Issues Submitted By Hour</Typography>
              <StatsBarChart
                data={data.hourCountData}
                xAxisKey={'hour'}
                barDataKeys={['oneDayAgo', 'currentDate']}
                barDataLabels={['Previous Day', 'Current Date']}
                colors={[colors.teal, colors.syfGreen]}
              />
            </Grid>
          )}
          <Grid item container justify="center" spacing={3}>
            <Grid item>
              <StatsTable
                rows={data.status}
                status={true}
                numIssues={data.numIssues}
                totalIncomplete={data.totalIncomplete}
                columns={['Status', 'Count', 'Percentage']}
              />
            </Grid>
            <Grid item>
              <StatsTable
                rows={data.priority}
                columns={['Priority', 'Count', 'Percentage']}
              />
            </Grid>
            <Grid item>
              <StatsTable
                rows={data.intake}
                columns={['Intake', 'Count', 'Percentage']}
              />
            </Grid>
            <Grid item>
              <StatsTable
                rows={data.issueTypes}
                columns={['Issue Types', 'Count', 'Percentage']}
              />
            </Grid>
            <Grid item>
              <StatsTable
                rows={data.location}
                columns={['Location', 'Count', 'Percentage']}
              />
            </Grid>
          </Grid>
        </Grid>
      )}
    </div>
  )
}
