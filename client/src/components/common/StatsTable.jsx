import React, { useState, useEffect } from 'react'
import {
  withStyles,
  makeStyles,
  createMuiTheme
} from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Box from '@material-ui/core/Box'
import { colors } from '../../styles/theme'

const StyledTableHeaderCell = withStyles((theme) => ({
  head: {
    backgroundColor: colors.steelGray,
    color: theme.palette.common.white
  },
  body: {
    fontSize: 14
  }
}))(TableCell)

const StyledTableCell = withStyles((theme) => ({
  root: {
    backgroundColor: colors.syfGold
  }
}))(TableCell)

const TotalStyledRow = withStyles((theme) => ({
  root: {
    backgroundColor: colors.syfGreen
  }
}))(TableRow)

const IncompleteRow = withStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.error.light
  }
}))(TableRow)

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary
  },
  tableContainer: {
    display: 'inline-block',
    minWidth: '400'
  }
}))

function getCompleteIndex(items) {
  for (var index = 0; index < items.length; index++) {
    if (items[index].name == 'Complete') {
      return index
    }
  }
  return 0
}
function GetTotalCount(items) {
  return items && items.map(({ count }) => count).reduce((sum, i) => sum + i, 0)
}

function GetStatusIncompleteCount(complete_count, count_total) {
  return count_total - complete_count
}

function GetStatusIncompletePercentage(complete_percentage) {
  return 100 - complete_percentage
}
const percentageFormat = (value) => value.toFixed(2)

function SortRows(array, attribute) {
  return array && array.sort((a, b) => (a[attribute] < b[attribute] ? 1 : -1))
}

export default function StatsTable(props) {
  const { rows, status, columns, totalIncomplete, numIssues } = props
  const [count_total, setCountTotal] = useState(0)
  const [sortedRows, setSortedRows] = useState([])
  const [percentIncomplete, setPercentIncomplete] = useState(0)
  const classes = useStyles()

  var statusIncompleteCount = 0
  var statusIncompletePercentage = 0.0

  useEffect(() => {
    if (rows && typeof rows[0] === 'object') {
      if (status && numIssues !== 0) {
        let percentage = (totalIncomplete / numIssues) * 100
        setPercentIncomplete(percentage)
      }

      setCountTotal(GetTotalCount(rows))
      let sortRows =
        columns[0].toLowerCase() === 'priority'
          ? rows
          : SortRows(rows, 'percentage')
      setSortedRows((oldRows) => sortRows)
    }
  }, [props])

  return (
    <TableContainer component={Paper} className={classes.tableContainer}>
      <Table size="small" aria-label="a dense table">
        <TableHead>
          <TableRow>
            {columns.map((column, index) => (
              <StyledTableHeaderCell
                align={index > 0 ? 'right' : 'inherit'}
                key={index}
              >
                {column}
              </StyledTableHeaderCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {sortedRows.map((row) => (
            <TableRow key={row.name}>
              <StyledTableCell component="th" scope="row">
                {row.name}
              </StyledTableCell>
              <TableCell className={classes.tableCell} align="right">
                {row.count}
              </TableCell>
              <TableCell className={classes.tableCell} align="right">
                {percentageFormat(row.percentage)}%
              </TableCell>
            </TableRow>
          ))}
          {status && (
            <IncompleteRow>
              <TableCell component="th" scope="row">
                Total Incomplete
              </TableCell>
              <TableCell align="right">{totalIncomplete}</TableCell>
              <TableCell align="right">
                {percentageFormat(percentIncomplete)}%
              </TableCell>
            </IncompleteRow>
          )}
          <TotalStyledRow>
            <TableCell component="th" scope="row">
              <Box fontWeight="fontWeightBold" m={1}>
                TOTAL
              </Box>
            </TableCell>
            <TableCell align="right">{count_total}</TableCell>
            <TableCell align="right"></TableCell>
          </TotalStyledRow>
        </TableBody>
      </Table>
    </TableContainer>
  )
}
