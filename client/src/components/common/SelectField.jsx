import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

import { Field, useFormikContext } from "formik";

const useStyles = makeStyles((theme) => ({
  formControl: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(1.25),
    minWidth: 170,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  label: {
    paddingLeft: theme.spacing(2),
  },
}));

export default function SelectField({
  name,
  label,
  options,
  labels,
  required,
  variant,
}) {
  const classes = useStyles();

  const { values, handleChange } = useFormikContext();

  return (
    <FormControl fullWidth className={classes.formControl} required={required}>
      <InputLabel className={classes.label} id="demo-simple-select-label">
        {label}
      </InputLabel>
      <Field
        name={name}
        id={name}
        component={Select}
        fullWidth
        required={required}
        variant={variant}
        onChange={handleChange(name)}
        value={values[name]}
      >
        {options &&
          options.map((item, index) => (
            <MenuItem key={index} value={item}>
              {item}
            </MenuItem>
          ))}
      </Field>
    </FormControl>
  );
}
