import React from "react";
import Button from "@material-ui/core/Button";
export default function CancelButton({ handleClose }) {
  return (
    <Button onClick={handleClose} color="secondary" variant="outlined">
      Cancel
    </Button>
  );
}
