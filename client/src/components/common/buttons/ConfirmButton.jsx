import React, { useState, useEffect } from "react";
import Button from "@material-ui/core/Button";
import { put } from "../../../api/requests";

export default function ConfirmButton({ status, handleClose, refresh, id, error }) {
  const handleClick = () => {
    if(status !== ''){
      put(`/api/issues/status/${id}`, { status: status })
      .then((res) => res.json())
      .then((res) => {
        if (res.status === "success") {
          handleClose();
          refresh();
        }
      });
    }else{
      error();
    }
  };

  return (
    <Button color="primary" variant="contained" onClick={handleClick}>
      Confirm
    </Button>
  );
}
