import React from "react";
import { Switch, Route, BrowserRouter as Router } from "react-router-dom";
import CreateIssue from "./pages/CreateIssue";
import EditIssues from "./pages/EditIssues";
import Error from "./pages/Error";
import Stats from "./pages/Stats";
import Resources from "./pages/Resources";

export default function App() {
  return (
    <Router>
      <Switch>
        <Route path="/" component={Resources} exact />
        <Route path="/add" component={CreateIssue} exact />
        <Route path="/edit/:countryId" component={EditIssues} exact />
        <Route path="/stats" component={Stats} exact />
        <Route component={Error} />
      </Switch>
    </Router>
  );
}
