import { get } from "../api/requests";
import React, { useState, useEffect } from "react";

export default function Options() {
  const [data, setData] = useState({});

  useEffect(() => {
    get("/api/form-options")
      .then((res) => res.json())
      .then((res) => {
        setData(res);
      });
  }, []);

  return data;
}
