export const req = {
  method: "",
  mode: "cors",
  cache: "no-cache",
  redirect: "follow",
  referrer: "no-referrer",
  headers: {
    "Content-Type": "application/json",
    Accept: "application/json, text/plain",
  },
};

export async function get(url) {
  req.method = "GET";

  if (req.body) {
    delete req.body;
  }

  return await fetch(url, req).then((res) => {
    return res;
  });
}

export async function post(url, body) {
  req.method = "POST";
  req.body = JSON.stringify(body);
  return await fetch(url, req).then((res) => {
    return res;
  });
}

export async function put(url, body) {
  req.method = "PUT";
  req.body = JSON.stringify(body);
  return await fetch(url, req).then((res) => {
    return res;
  });
}
