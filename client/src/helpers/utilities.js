import {colors} from '../styles/theme'
export const countries = [
  {
    name: "All",
    id: 0,
    value: "",
  },
  {
    name: "USA",
    id: 1,
    value: "usa",
  },
  {
    name: "Philippines",
    id: 2,
    value: "Philippines",
  },
  {
    name: "India",
    id: 3,
    value: "India",
  },
];
export const setRowProps = (row = "") => {
  // handle null
  if (row) {
    if (row.toLowerCase() == "unassigned") {
      return {
          backgroundColor: colors.white,
      };
    } else if (row.toLowerCase() == "in progress") {
      return {
          backgroundColor: colors.lightYellow,
      };
    } else if (row.toLowerCase() == "first attempt") {
      return {
          backgroundColor: colors.lightPink,
      };
    } else if (row.toLowerCase() == "second attempt") {
      return {
          backgroundColor: colors.pink,
      };
    } else if (row.toLowerCase() === "complete") {
      return {
          backgroundColor: colors.syfGreen,
      };
    } else {
      return {
          backgroundColor: colors.steelGray,
      };
    }
  }
}

export const formatPhoneNumber = (str) => {
  //Filter only numbers from the input
  let cleaned = ('' + str).replace(/\D/g, '');
  
  //Check if the input is of correct length
  let match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);

  if (match) {
    return '(' + match[1] + ') ' + match[2] + '-' + match[3]
  };

  return null
};
export const employeeTypes = [
  {
    name: "All",
    id: "0",
    value: "",
  },
  {
    name: "Full Time",
    id: "1",
    value: "ft",
  },
  {
    name: "Contractor",
    id: "2",
    value: "contractor",
  },
];

export const getObjectInfo = (
  objectArray,
  knownValue,
  compareTo,
  lookingFor
) => {
  const knownObject = objectArray.find(
    (object) => object[knownValue] == compareTo
  );
  return knownObject[lookingFor];
};

export const validator = (requiredFieldsObject, values) => {
  let errors = {};
  Object.keys(requiredFieldsObject).map((key) => {
    if (!values[key]) {
      errors[key] = true;
    } else {
      errors[key] = false;
    }
  });
  return Object.keys(errors).reduce((acc, key)=> acc || errors[key], false)
};

export const removeDoubleQuotes = (values) => {
  Object.keys(values).map((key) => {
    if (values[key]) {
      if (typeof values[key] === "string") {
        values[key] = values[key].replace(/'/g, '"');
      }
    }
  });
  return values;
};
