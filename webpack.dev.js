const merge = require("webpack-merge");
const common = require("./webpack.common.js");
var path = require("path");

module.exports = merge(common, {
  mode: "development",
  devServer: {
    contentBase: path.join(__dirname, "dist/build"),
    port: 3000,
    hot: true,
    overlay: {
      warnings: true,
      errors: true
    },
    watchContentBase: true,
    historyApiFallback: true,
    proxy: {
      context: ["/api"],
      target: "http://localhost:8000",
      secure: false
    }
  }
});
